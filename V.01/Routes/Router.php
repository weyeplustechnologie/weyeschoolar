<?php
namespace Router;

class Router 
{
    public $url;
    public $routes=[];

    public function __construct($url)
    {
        $this->url=trim($url,"/");
        
    }
    public function get(string $path, $action){
        $route=new Route($path, $action);
        $this->routes["GET"][]=$route;
    }
    public function post(string $path, $action){
        $route=new Route($path, $action);
        $this->routes["POST"][]=$route;
    }
    public function run(){
       foreach($this->routes[$_SERVER['REQUEST_METHOD']] as $route){
        if($route->matches($this->url)){

            $route->execute();
           
        }
       
       }
      echo( json_encode(array("404"=>"Service not exist")));
    }
    
}


?>