<?php


namespace App\Controllers;

use App\Models\Dao\DaoAdresse;
use App\Models\Dao\DaoAgent;
use App\Models\Dao\DaoAnnee;
use App\Models\Dao\DaoEcole;
use App\Models\Dao\DaoFonctionEmploie;
use App\Models\Dao\DaoPersonne;
use App\Models\Entity\Adresse;
use App\Models\Entity\Agent;
use App\Models\Entity\FonctionEmploie;
use App\Models\Entity\Personne;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class AgentRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
       $unepersonne= new Personne();
       $unDaoPersonne= new DaoPersonne();
       $uneAdresse= new Adresse();
       $unDaoAdresse= new DaoAdresse();
      $uneFonctionEmploye= new FonctionEmploie();
      $unDaoFonctionEmploye= new DaoFonctionEmploie();
      $unDaoAnnee= new DaoAnnee();
     
        $unAgent= new Agent();
        $unDaoAgent= new DaoAgent();
         
        $uneAdresse->setAvenue($this->httpParam->getHttpParam()["personne"]["adresse"]["avenue"]);
        $uneAdresse->setCommune($this->httpParam->getHttpParam()["personne"]["adresse"]["commune"]);
        $uneAdresse->setNumero($this->httpParam->getHttpParam()["personne"]["adresse"]["numero"]);
        $uneAdresse->setQuartier($this->httpParam->getHttpParam()["personne"]["adresse"]["quartier"]);
        $uneAdresse->setVille($this->httpParam->getHttpParam()["personne"]["adresse"]["ville"]);
        $idadresse=$unDaoAdresse->create($uneAdresse);

      $unepersonne->setNom($this->httpParam->getHttpParam()["personne"]["nom"]);
      $unepersonne->setPostnom($this->httpParam->getHttpParam()["personne"]["postnom"]);
      $unepersonne->setPrenom($this->httpParam->getHttpParam()["personne"]["prenom"]);
      $unepersonne->setSexe($this->httpParam->getHttpParam()["personne"]["sexe"]);
      $unepersonne->setLieuNaissance($this->httpParam->getHttpParam()["personne"]["lieuNaissance"]);
      $unepersonne->setNationalite($this->httpParam->getHttpParam()["personne"]["nationalite"]);
      $unepersonne->setProvince($this->httpParam->getHttpParam()["personne"]["province"]);
      $unepersonne->setDateNaissance($this->httpParam->getHttpParam()["personne"]["dateNaissance"]);
      $unepersonne->setIdAdresse($idadresse);
      $idpersonne=$unDaoPersonne->create($unepersonne);

 

      $unAgent->setMatriculeAgent($this->httpParam->getHttpParam()["Agent"]["matricule"]);
      $unAgent->setNiveauEtude($this->httpParam->getHttpParam()["Agent"]["niveauEtude"]);
      $unAgent->setPhone($this->httpParam->getHttpParam()["Agent"]["phone"]);
      $unAgent->setCivilite($this->httpParam->getHttpParam()["Agent"]["civilite"]);
      $unAgent->setIdPersonne($idpersonne);
      $unDaoAgent->create($unAgent);

      $uneFonctionEmploye->setMatriculeAgent($this->httpParam->getHttpParam()["Agent"]["matricule"]);
      $uneFonctionEmploye->setIdFonction($this->httpParam->getHttpParam()["fonction"]);
      $uneFonctionEmploye->setIdAnnee($unDaoAnnee->findAnneeOuverte()[0]["Id_annee"]);
      $unDaoFonctionEmploye->create($uneFonctionEmploye);
          
     $this->returnResponse(SUCCESS_RESPONSE, "true" );
     

   } 
   public function getAll(){
    $unDaoAgent= new DaoAgent();
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoAgent->findAll());
   }
   public function getAllAgentbyFonction($id){
    $unDaoAgent= new DaoAgent();
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoAgent->findAllAgentByFonction($id));
   }
   public function getMatricule(){
    $unDaoAgent= new DaoAgent();
    $unDaoAnnee= new DaoAnnee();
    $unDaoEcole= new DaoEcole();
 

    $indiceAnnee= $unDaoAnnee->findAnneeOuverte();
    $indiceEcole= $unDaoEcole->findAll();
    $indiceEcole= strtoupper(substr($indiceEcole[0]["nom"],0,2));
    $indiceAnnee=substr($indiceAnnee[0]["libelleAnnee"],2,2);
    $num=$unDaoAgent->Count()[0]["total"];
    if($num<9 ){
        $num=$num+1;
        $num= "00".$num;
    }
    else if(($num==9) && ($num<99)){
        $num=$num+1;
        $num= "0".$num;
    }
    else{
        $num=$num+1;
        $num= "".$num;
    }
      $matricule=$indiceEcole."A".$num."/".$indiceAnnee;

      $this->returnResponse(SUCCESS_RESPONSE, $matricule);
   }

  

}


?>