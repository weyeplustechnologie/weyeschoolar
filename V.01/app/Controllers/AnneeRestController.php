<?php


namespace App\Controllers;

use App\Models\Dao\DaoAnnee;
use App\Models\Entity\Ecole;
use App\Models\Dao\DaoEcole;
use App\Models\Entity\Annee;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class AnneeRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $uneAnnee= new Annee();
      $unDaoAnnee= new DaoAnnee();
      $uneAnnee->setLibelleAnnee($this->httpParam->getHttpParam()["libelleAnnee"]);
      $uneAnnee->setDateDebut($this->httpParam->getHttpParam()["datedebutAnnee"]);
      $uneAnnee->setDateFin($this->httpParam->getHttpParam()["datefinAnnee"]);
      $uneAnnee->setStatut($this->httpParam->getHttpParam()["statutAnnee"]);
   
        
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoAnnee->create($uneAnnee));
     

   } 
   public function getAll(){
    $unDaoAnnee= new DaoAnnee();
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoAnnee->findAll());
   }

  

}


?>