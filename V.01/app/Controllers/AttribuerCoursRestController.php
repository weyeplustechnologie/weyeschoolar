<?php


namespace App\Controllers;

use App\Models\Dao\DaoClasse;
use App\Models\Dao\DaoCours;
use App\Models\Dao\DaoDispensecours;
use App\Models\Entity\Cours;
use App\Models\Entity\Dispensecours;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class AttribuerCoursRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $unDispenseCours= new Dispensecours();
      $unDaoDispenseCours= new DaoDispensecours();

      $unDispenseCours->setIdAnnee($this->httpParam->getHttpParam()["anneeattribuerCours"]);
      $unDispenseCours->setIdClasse($this->httpParam->getHttpParam()["classeattribuerCours"]);
      $unDispenseCours->setIdCours($this->httpParam->getHttpParam()["coursattribuerCours"]);
      $unDispenseCours->setMatriculeAgent($this->httpParam->getHttpParam()["agentattribuerCours"]);
      
        
   
        
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoDispenseCours->create($unDispenseCours));
     

   } 
   public function getAll(){
    $unDaoDispenseCours= new DaoDispensecours();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoDispenseCours->findAll());
   }

  

}


?>