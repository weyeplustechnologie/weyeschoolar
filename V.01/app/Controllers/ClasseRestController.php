<?php


namespace App\Controllers;

use App\Models\Dao\DaoClasse;
use App\Models\Dao\DaoDivision;
use App\Models\Entity\Classe;
use App\Models\Entity\Division;
use App\Models\Entity\Option;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class ClasseRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $uneClasse= new Classe();
      $unDaoclasse= new DaoClasse();
      $uneClasse->setLibelleClasse($this->httpParam->getHttpParam()["libelleClasse"]);
      $uneClasse->setIdCycle($this->httpParam->getHttpParam()["cycleClasse"]);
      $uneClasse->setIdSection($this->httpParam->getHttpParam()["sectionClasse"]);
      $uneClasse->setIdOption($this->httpParam->getHttpParam()["optionClasse"]);
      $uneClasse->setIdDivision($this->httpParam->getHttpParam()["divisionClasse"]);
        
   
        
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoclasse->create($uneClasse));
     

   } 
   public function getAll(){
    $unDaoclasse= new DaoClasse();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoclasse->findAll());
   }

  

}


?>