<?php


namespace App\Controllers;

use App\Models\Dao\DaoClasse;
use App\Models\Dao\DaoCours;
use App\Models\Entity\Cours;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class CoursRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $unCours= new Cours();
      $unDaoCours= new DaoCours();
      $unCours->setLibelleCours($this->httpParam->getHttpParam()["libelleCours"]);
      $unCours->setNoteGeneral($this->httpParam->getHttpParam()["noteCours"]);
      $unCours->setPonderation($this->httpParam->getHttpParam()["ponderationCours"]);
      $unCours->setIdCycle($this->httpParam->getHttpParam()["idcycle"]);
      $unCours->setIdSection($this->httpParam->getHttpParam()["idsection"]);
      $unCours->setIdOption($this->httpParam->getHttpParam()["idoption"]);
   
        
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoCours->create($unCours));
     

   } 
   public function getAll(){
    $unDaoCours= new DaoCours();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoCours->findAll());
   }

  

}


?>