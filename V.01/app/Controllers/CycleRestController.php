<?php


namespace App\Controllers;


use App\Models\Dao\DaoCycle;
use App\Models\Entity\Cycle;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class CycleRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $unCycle= new Cycle();
      $unDaoCycle= new DaoCycle();
      $unCycle->setLibelleCycle($this->httpParam->getHttpParam()["libelleCycle"]);
      $unCycle->setDescription($this->httpParam->getHttpParam()["descriptionCycle"]);
     
   
        
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoCycle->create($unCycle));
     

   } 
   public function getAll(){
    $unDaoCycle= new DaoCycle();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoCycle->findAll());
   }

  

}


?>