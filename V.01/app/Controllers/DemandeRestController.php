<?php


namespace App\Controllers;
use App\Models\Entity\DemandeCoin;
use App\Models\Dao\DaoDemande;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;
use App\PHPMailer\PHPMailer;
use App\PHPMailer\Exception;
use App\PHPMailer\SMTP;


class DemandeRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $uneDemande= new DemandeCoin();
      $unDaoDemande= new DaoDemande();
      $uneDemande->setPhone($this->httpParam->getHttpParam()["phone"]);
      $uneDemande->setAdresseWallet($this->httpParam->getHttpParam()["wallet"]);
      $uneDemande->setMontantPaye($this->httpParam->getHttpParam()["montPaye"]);
      $uneDemande->setCoinPaie($this->httpParam->getHttpParam()["coinPaie"]);
      
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoDemande->create($uneDemande));
     

   } 
   public function getInactif(){
      $unDaoDemande= new DaoDemande();
     
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoDemande->findOnebyStatutInActive());
   }
   public function updateStatut($id){
      $unDaoDemande= new DaoDemande();
      $uneDemande= new DemandeCoin();
      $uneDemande->setStatut($this->httpParam->getHttpParam()["statut"]);
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoDemande->updateDemandeConfirmer($id,$uneDemande));
   }
   public function updateReference($id){
      $unDaoDemande= new DaoDemande();
      $uneDemande= new DemandeCoin();
      $uneDemande->setRefTransaction($this->httpParam->getHttpParam()["ref"]);
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoDemande-> updateDemande($id,$uneDemande));
   }
   public function sendMailer(){

      $email=$this->httpParam->getHttpParam()["mail"];
      $name=$this->httpParam->getHttpParam()["name"];
      $subject=$this->httpParam->getHttpParam()["subject"];
      $body=$this->httpParam->getHttpParam()["body"];

      $mail= new PHPMailer();
      $mail->isSMTP();
      $mail->Host="smtp.gmail.com";
      $mail->SMTPAuth=true;
      $mail->Username="juguellay@gmail.com";
      $mail->Password="boslay12";
      $mail->Portt=465;
      

      $mail->isHTML(true);
      $mail->setFrom($email,$name);
      $mail->addAddress("juguellay@gmail.com");
      $mail->Subject=("$email ($subject)");
      $mail->Body=$body;

      if($mail->send()){
         $this->returnResponse(SUCCESS_RESPONSE, "Mail envoyé");
      }
      else{
         $this->returnResponse(SUCCESS_RESPONSE, $mail);
      }

   }
   public function delete(){

   }
   public function updateById($id){
      $uneDemande=new Demandecoin();
      $unDaoDemande=new DaoDemande();
      $uneDemande->setRefTransaction($this->httpParam->getHttpParam()["refTrans"]);
      $uneDemande->setStatut($this->httpParam->getHttpParam()["statuts"]);
    
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoDemande->updateDemande($id,$uneDemande));
     
   }
  

}


?>