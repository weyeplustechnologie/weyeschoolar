<?php


namespace App\Controllers;

use App\Models\Dao\DaoDivision;
use App\Models\Entity\Division;
use App\Models\Entity\Option;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class DivisionRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $uneDivision= new Division();
      $unDaoDivision= new DaoDivision();
      $uneDivision->setLibelleDivision($this->httpParam->getHttpParam()["libelleDivision"]);
     
   
        
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoDivision->create($uneDivision));
     

   } 
   public function getAll(){
    $unDaoDivision= new DaoDivision();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoDivision->findAll());
   }

  

}


?>