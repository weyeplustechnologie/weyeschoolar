<?php


namespace App\Controllers;

use App\Models\Dao\DaoAdresse;
use App\Models\Entity\Ecole;
use App\Models\Dao\DaoEcole;
use App\Models\Entity\Adresse;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class EcoleRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $uneEcole= new Ecole();
      $unDaoEcole= new DaoEcole();
      $uneAdresse= new Adresse();
      $unDaoAdresse= new DaoAdresse();
      
      $uneAdresse->setAvenue($this->httpParam->getHttpParam()["adresse"]["avenue"]);
        $uneAdresse->setCommune($this->httpParam->getHttpParam()["adresse"]["commune"]);
        $uneAdresse->setNumero($this->httpParam->getHttpParam()["adresse"]["numero"]);
        $uneAdresse->setQuartier($this->httpParam->getHttpParam()["adresse"]["quartier"]);
        $uneAdresse->setVille($this->httpParam->getHttpParam()["adresse"]["ville"]);
        $idadresse=$unDaoAdresse->create($uneAdresse);

      $uneEcole->setNomEcole($this->httpParam->getHttpParam()["nom"]);
      $uneEcole->setPromteur($this->httpParam->getHttpParam()["Promoteur"]);
      $uneEcole->setDevise($this->httpParam->getHttpParam()["deviseEcole"]);
      $uneEcole->setIdAdresse($idadresse);
      $uneEcole->setTypeEcole($this->httpParam->getHttpParam()["typeEcole"]);
      $uneEcole->setLogo($this->httpParam->getHttpParam()["logo"]);
      
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoEcole->create($uneEcole));
     

   } 
   public function getAll(){
      $unDaoEcole= new DaoEcole();
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoEcole->findAll());
   }

  

}


?>