<?php


namespace App\Controllers;

use App\Models\Dao\DaoAdresse;
use App\Models\Dao\DaoAnnee;
use App\Models\Dao\DaoEcole;
use App\Models\Dao\DaoEleve;
use App\Models\Dao\DaoInscription;
use App\Models\Dao\DaoPersonne;
use App\Models\Entity\Adresse;
use App\Models\Entity\Eleve;
use App\Models\Entity\Inscription;
use App\Models\Entity\Personne;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class EleveRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
       $unepersonne= new Personne();
       $unDaoPersonne= new DaoPersonne();
       $uneAdresse= new Adresse();
       $unDaoAdresse= new DaoAdresse();
       $unEleve= new Eleve();
       $unDaoEleve= new DaoEleve();
     
       $uneInscription= new Inscription();
       $unDaoInscription= new DaoInscription();

        $unDaoAnnee= new DaoAnnee();
     
         
        $uneAdresse->setAvenue($this->httpParam->getHttpParam()["personne"]["adresse"]["avenue"]);
        $uneAdresse->setCommune($this->httpParam->getHttpParam()["personne"]["adresse"]["commune"]);
        $uneAdresse->setNumero($this->httpParam->getHttpParam()["personne"]["adresse"]["numero"]);
        $uneAdresse->setQuartier($this->httpParam->getHttpParam()["personne"]["adresse"]["quartier"]);
        $uneAdresse->setVille($this->httpParam->getHttpParam()["personne"]["adresse"]["ville"]);
        $idadresse=$unDaoAdresse->create($uneAdresse);

      $unepersonne->setNom($this->httpParam->getHttpParam()["personne"]["nom"]);
      $unepersonne->setPostnom($this->httpParam->getHttpParam()["personne"]["postnom"]);
      $unepersonne->setPrenom($this->httpParam->getHttpParam()["personne"]["prenom"]);
      $unepersonne->setSexe($this->httpParam->getHttpParam()["personne"]["sexe"]);
      $unepersonne->setLieuNaissance($this->httpParam->getHttpParam()["personne"]["lieuNaissance"]);
      $unepersonne->setNationalite($this->httpParam->getHttpParam()["personne"]["nationalite"]);
      $unepersonne->setProvince($this->httpParam->getHttpParam()["personne"]["province"]);
      $unepersonne->setDateNaissance($this->httpParam->getHttpParam()["personne"]["dateNaissance"]);
      $unepersonne->setIdAdresse($idadresse);
      $idpersonne=$unDaoPersonne->create($unepersonne);

 
        
      $unEleve->setMatriculeEleve($this->httpParam->getHttpParam()["eleve"]["matricule"]);
      $unEleve->setTuteur($this->httpParam->getHttpParam()["eleve"]["nomTuteur"]);
      $unEleve->setStatutTuteur($this->httpParam->getHttpParam()["eleve"]["statutTuteur"]);
      $unEleve->setPhoneTuteur($this->httpParam->getHttpParam()["eleve"]["phoneTuteur"]);
      $unEleve->setIdPersonne($idpersonne);
      $unEleve->setIdAnnee($unDaoAnnee->findAnneeOuverte()[0]["Id_annee"]);
      $unDaoEleve->create($unEleve);

      $uneInscription->setMatriculeEleve($this->httpParam->getHttpParam()["eleve"]["matricule"]);
      $uneInscription->setStatutInscription("1");
      $uneInscription->setIdAnnee($unDaoAnnee->findAnneeOuverte()[0]["Id_annee"]);
      $uneInscription->setIdClasse($this->httpParam->getHttpParam()["inscription"]["classe"]);
      $uneInscription->setIdCycle($this->httpParam->getHttpParam()["inscription"]["cycle"]);
      $uneInscription->setIdOption($this->httpParam->getHttpParam()["inscription"]["option"]);
      $uneInscription->setIdSection($this->httpParam->getHttpParam()["inscription"]["section"]);
      $uneInscription->setIdExtension($this->httpParam->getHttpParam()["inscription"]["extension"]);

      $unDaoInscription->create($uneInscription);
          
     $this->returnResponse(SUCCESS_RESPONSE, "true" );
     

   } 
   public function getAllActive(){
        $unDaoInscription= new DaoInscription();
        $this->returnResponse(SUCCESS_RESPONSE, $unDaoInscription->findAllActive());
   }

   public function getMatricule(){
    $unDaoInscription= new DaoInscription();
    $unDaoAnnee= new DaoAnnee();
    $unDaoEcole= new DaoEcole();
 

    $indiceAnnee= $unDaoAnnee->findAnneeOuverte();
    $indiceEcole= $unDaoEcole->findAll();
    $indiceEcole= strtoupper(substr($indiceEcole[0]["nom"],0,2));
    $indiceAnnee=substr($indiceAnnee[0]["libelleAnnee"],2,2);
    $num=$unDaoInscription->Count()[0]["total"];
    if($num<9 ){
        $num=$num+1;
        $num= "00".$num;
    }
    else if(($num==9) && ($num<99)){
        $num=$num+1;
        $num= "0".$num;
    }
    else{
        $num=$num+1;
        $num= "".$num;
    }
      $matricule=$indiceEcole."E".$num."/".$indiceAnnee;

      $this->returnResponse(SUCCESS_RESPONSE, $matricule);
   }

  

}


?>