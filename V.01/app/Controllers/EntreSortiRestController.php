<?php


namespace App\Controllers;

use App\Models\Dao\DaoAdresse;
use App\Models\Dao\DaoAnnee;
use App\Models\Dao\DaoEntreSortie;
use App\Models\Entity\EntreSortie;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class EntreSortiRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
       $uneEntreeSortie= new EntreSortie();
       $unDaoEntreeSortie= new DaoEntreSortie();
        $unDaoAnnee= new DaoAnnee();
     
                
        $uneEntreeSortie->setDateEntresortie($this->httpParam->getHttpParam()["datepaiement"]);
        $uneEntreeSortie->setMontant($this->httpParam->getHttpParam()["montant"]);
        $uneEntreeSortie->setMotif($this->httpParam->getHttpParam()["motif"]);
        $uneEntreeSortie->setIdAnnee($unDaoAnnee->findAnneeOuverte()[0]["Id_annee"]);
        $uneEntreeSortie->setIdExtension($this->httpParam->getHttpParam()["extension"]);
        $uneEntreeSortie->setIdUser($this->httpParam->getHttpParam()["user"]);
        $uneEntreeSortie->setTypeOperation($this->httpParam->getHttpParam()["typeentresortie"]);
        $uneEntreeSortie->setConcerne($this->httpParam->getHttpParam()["concerner"]);
        $uneEntreeSortie->setObservation($this->httpParam->getHttpParam()["observation"]);
        
        
      
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoEntreeSortie->create($uneEntreeSortie));
     

   } 


  

}


?>