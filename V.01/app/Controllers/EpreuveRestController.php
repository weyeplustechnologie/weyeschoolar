<?php


namespace App\Controllers;


use App\Models\Dao\DaoTypesSession;
use App\Models\Entity\TypeSession;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class EpreuveRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $untypeSession= new TypeSession();
      $unDaotypeSession= new DaoTypesSession();
      $untypeSession->setLibelleTypeSession($this->httpParam->getHttpParam()["libelleSession"]);
      $untypeSession->setIdCycle($this->httpParam->getHttpParam()["cycleSession"]);
     
      
        
   
        
     $this->returnResponse(SUCCESS_RESPONSE,  $unDaotypeSession->create($untypeSession));
     

   } 
   public function getAll(){
    $unDaotypeSession= new DaoTypesSession();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaotypeSession->findAll());
   }

  

}


?>