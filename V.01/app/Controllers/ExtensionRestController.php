<?php


namespace App\Controllers;

use App\Models\Dao\DaoExtension;
use App\Models\Entity\Extension;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class ExtensionRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $uneExtension= new Extension();
      $unDaoExtension= new DaoExtension();
      $uneExtension->setLibelleExtension($this->httpParam->getHttpParam()["libelleExtenstion"]);
      $uneExtension->setDescriptionExtension($this->httpParam->getHttpParam()["descriptionExtension"]);
      $uneExtension->setIdECole($this->httpParam->getHttpParam()["ecoleExtension"]);
     $this->returnResponse(SUCCESS_RESPONSE,  $unDaoExtension->create($uneExtension));

   } 
   public function getAll(){
     $unDaoExtension= new DaoExtension();
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoExtension->findAll());
   }

  

}


?>