<?php


namespace App\Controllers;

use App\Models\Dao\DaoFonction;
use App\Models\Entity\Fonction;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class FonctionRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
    
   public function add(){
      $uneFonction= new Fonction();
      $unDaoFonction= new DaoFonction();
      $uneFonction->setLibelleFonction($this->httpParam->getHttpParam()["libelleFonction"]);
      $uneFonction->setDescriptionFonction($this->httpParam->getHttpParam()["descriptionFonction"]);
     
   
        
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoFonction->create($uneFonction));
     

   } 
   public function getAll(){
      $unDaoFonction= new DaoFonction();
      $this->returnResponse(SUCCESS_RESPONSE, $unDaoFonction->findAll());
   }

  

}


?>