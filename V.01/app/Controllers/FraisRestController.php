<?php


namespace App\Controllers;

use App\Models\Dao\DaoFrais;
use App\Models\Entity\Frais;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class FraisRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $unFrais= new Frais();
      $unDaoFrais= new DaoFrais();
          
      $unFrais->setMontant($this->httpParam->getHttpParam()["libelleMontantFrais"]);
      $unFrais->setDevise($this->httpParam->getHttpParam()["deviseFrais"]);
      $unFrais->setIdAnnee($this->httpParam->getHttpParam()["anneeFrais"]);
      $unFrais->setIdTypeFrais($this->httpParam->getHttpParam()["typefraisFrais"]);
      $unFrais->setIdCycle($this->httpParam->getHttpParam()["cycle"]);
        
     $this->returnResponse(SUCCESS_RESPONSE,   $unDaoFrais->create($unFrais));
   } 
   public function getAll(){
    $unDaoFrais= new DaoFrais();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoFrais->findAll());
   }


}


?>