<?php


namespace App\Controllers;

use App\Models\Dao\DaoOption;
use App\Models\Entity\Option;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class OptionRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $uneOption= new Option();
      $unDaoOption= new DaoOption();
      $uneOption->setLibelleOption($this->httpParam->getHttpParam()["libelleOption"]);
      $uneOption->setDescriptionOption($this->httpParam->getHttpParam()["descriptionOption"]);
      $uneOption->setIdSection($this->httpParam->getHttpParam()["sectionOption"]);
   
        
     $this->returnResponse(SUCCESS_RESPONSE,$unDaoOption->create($uneOption));
     

   } 
   public function getAll(){
    $unDaooption= new DaoOption();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaooption->findAll());
   }

  

}


?>