<?php


namespace App\Controllers;

use App\Models\Dao\DaoAdresse;
use App\Models\Dao\DaoAnnee;
use App\Models\Dao\DaoEcole;
use App\Models\Dao\DaoEleve;
use App\Models\Dao\DaoInscription;
use App\Models\Dao\DaoPaiementFrais;
use App\Models\Dao\DaoPersonne;
use App\Models\Entity\Adresse;
use App\Models\Entity\Eleve;
use App\Models\Entity\Inscription;
use App\Models\Entity\PaiementFrais;
use App\Models\Entity\Personne;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class PaiementFraisRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
       $unpaiementFrais= new PaiementFrais();
       $unDaopaiementFrais= new DaoPaiementFrais();
        $unDaoAnnee= new DaoAnnee();
     
             
        $unpaiementFrais->setDatePaiement($this->httpParam->getHttpParam()["datepaiement"]);
        $unpaiementFrais->setMontant($this->httpParam->getHttpParam()["montantpaiement"]);
        $unpaiementFrais->setMatriculeEleve($this->httpParam->getHttpParam()["matriculeEleve"]);
        $unpaiementFrais->setIdTypefrais($this->httpParam->getHttpParam()["typefraispaiement"]);
        $unpaiementFrais->setIdPaiementtranche($this->httpParam->getHttpParam()["tranchepaiement"]);
        $unpaiementFrais->setIdAnnee($unDaoAnnee->findAnneeOuverte()[0]["Id_annee"]);
        $unpaiementFrais->setIdUsers($this->httpParam->getHttpParam()["user"]);
        $unpaiementFrais->setIdCycle($this->httpParam->getHttpParam()["cycle"]);
        $unpaiementFrais->setDevise($this->httpParam->getHttpParam()["devisepaiement"]);
        $unpaiementFrais->setIdExtension($this->httpParam->getHttpParam()["extension"]);
        
        

      
     $this->returnResponse(SUCCESS_RESPONSE, $unDaopaiementFrais->create($unpaiementFrais));
     

   } 


  

}


?>