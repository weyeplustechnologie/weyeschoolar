<?php


namespace App\Controllers;

use App\Models\Dao\DaoPaiementTranche;
use App\Models\Entity\PaiementTranche;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class PaiementTrancheFraisRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $unpaiementTranche= new PaiementTranche();
      $unDaoPaiementTranche= new DaoPaiementTranche();
          
      $unpaiementTranche->setMontant($this->httpParam->getHttpParam()["libelletranchemontant"]);
      $unpaiementTranche->setDevise($this->httpParam->getHttpParam()["deviseTrancheFrais"]);
      $unpaiementTranche->setIdAnnee($this->httpParam->getHttpParam()["anneeTrancheFrais"]);
      $unpaiementTranche->setIdTypeFrais($this->httpParam->getHttpParam()["typetranchefraisFrais"]);
      $unpaiementTranche->setIdTranche($this->httpParam->getHttpParam()["typetranchefrais"]);
      $unpaiementTranche->setIdCycle($this->httpParam->getHttpParam()["cycleTrancheFrais"]);
        
     $this->returnResponse(SUCCESS_RESPONSE,    $unDaoPaiementTranche->create($unpaiementTranche));
   } 
   public function getAll(){
    $unDaoPaiementTranche= new DaoPaiementTranche();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoPaiementTranche->findAll());
   }


}


?>