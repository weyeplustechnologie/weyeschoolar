<?php


namespace App\Controllers;


use App\Models\Dao\DaoCycle;
use App\Models\Dao\DaoSection;
use App\Models\Entity\Cycle;
use App\Models\Entity\Section;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class SectionRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $unSection= new Section();
      $unDaoSection= new DaoSection();
      $unSection->setLibelleSection($this->httpParam->getHttpParam()["libelleSection"]);
      $unSection->setDescriptionSection($this->httpParam->getHttpParam()["descriptionSection"]);
      $unSection->setIdCyle($this->httpParam->getHttpParam()["cycleSection"]);
   
        
     $this->returnResponse(SUCCESS_RESPONSE,$unDaoSection->create($unSection));
     

   } 
   public function getAll(){
    $unDaoSection= new DaoSection();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoSection->findAll());
   }

  

}


?>