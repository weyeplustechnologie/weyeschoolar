<?php


namespace App\Controllers;

use App\Models\Dao\DaoTranche;
use App\Models\Dao\DaoTypeFrais;
use App\Models\Dao\DaoTypesSession;
use App\Models\Entity\Tranche;
use App\Models\Entity\TypeFrais;
use App\Models\Entity\TypeSession;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class TrancheFraisRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $unTranche= new Tranche();
      $unDaoTranche= new DaoTranche();
      $unTranche->setLibelleTranche($this->httpParam->getHttpParam()["libelleTrancheFrais"]);
      $unTranche->setIdTypeFrais($this->httpParam->getHttpParam()["typefraisTranche"]);
        
     $this->returnResponse(SUCCESS_RESPONSE,  $unDaoTranche->create($unTranche));
   } 
   public function getAll(){
    $unDaoTranche= new DaoTranche();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoTranche->findAll());
   }


}


?>