<?php


namespace App\Controllers;

use App\Models\Dao\DaoTypeFrais;
use App\Models\Dao\DaoTypesSession;
use App\Models\Entity\TypeFrais;
use App\Models\Entity\TypeSession;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;



class TypeFraisRestController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
 
   public function add(){
      $untypeFrais= new TypeFrais();
      $unDaotypeFrais= new DaoTypeFrais();
      $untypeFrais->setLibelleFrais($this->httpParam->getHttpParam()["libelleTypeFrais"]);
        
     $this->returnResponse(SUCCESS_RESPONSE,  $unDaotypeFrais->create($untypeFrais));
   } 
   public function getAll(){
    $unDaotypeFrais= new DaoTypeFrais();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaotypeFrais->findAll());
   }


}


?>