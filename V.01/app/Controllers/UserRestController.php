<?php
namespace App\Controllers;


use App\Models\Entity\User;
use App\Models\Dao\DaoUser;
use Router\HttpParam;
use Router\DataOut;
use App\Utils\Constant;

class UserRESTController extends DataOut
{
   public $httpParam;

   public function __construct()
   {
    new Constant();  
    $this->httpParam=new HttpParam();
   }
   public function add(){
      $unUser= new User();
      $unDaoUser= new DaoUser();
      $unUser->setIdentifiant($this->httpParam->getHttpParam()["Identifiant"]);
      $unUser->setPassword($this->httpParam->getHttpParam()["motdepasse"]);
      $unUser->setMatricule($this->httpParam->getHttpParam()["agentmatricule"]);
      $unUser->setIdExtension($this->httpParam->getHttpParam()["extensionUtilisateur"]);
      $unUser->setCreateBy($this->httpParam->getHttpParam()["createdby"]);
      $unUser->setRole($this->httpParam->getHttpParam()["role"]);
      $unUser->setStatut(true);
      $unUser->setCreateOn(date("Y-m-d H:i:s"));
     
      
     $this->returnResponse(SUCCESS_RESPONSE, $unDaoUser->create($unUser));
     

   } 
   public function login(){
      $unUser= new User();
      $unDaoUser= new DaoUser();
      $unUser->setIdentifiant($this->httpParam->getHttpParam()["userKey"]);
      $unUser->setPassword($this->httpParam->getHttpParam()["passKey"]);
      $conn= $unDaoUser->checkUser($unUser);
     
      if(!is_array($conn)){
         $this->returnResponse(INVALID_USER_PASS, "Identifiant soit Mot de passe est  incorrect");
         }
         if($conn["statut"]==0){
            $this->returnResponse(USER_NOT_ACTIVE, " Cet utilisateur est bloqué. Veuillez contacter l'administrateur");
         }
         
         $this->returnResponse(SUCCESS_RESPONSE,$conn);  
  
     

   }
   public function getAll(){
      $unDaoUser= new DaoUser();
      $this->returnResponse(SUCCESS_RESPONSE,  $unDaoUser->findAll());
   }

}


?>