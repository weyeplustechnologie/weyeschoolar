<?php
namespace App\Models;

    class  BdConnect
    {
        private $server ="localhost";
        private $bdname="weyescholar";
        private $user= "root";
        private $pass="";

        public function connect()
        {
            try {
                $conn= new \PDO("mysql:host=".$this->server.";dbname=".$this->bdname, $this->user, $this->pass);
                $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                return $conn;
            } catch (\Exception $e) {
                echo "DataBase Error:". $e->getMessage();
            }
            

        } 
        
    }
    

    
?>
