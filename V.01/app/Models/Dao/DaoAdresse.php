<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Adresse;

class DaoAdresse implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($adresse){
        $ville=$adresse->getVille();
        $commune=$adresse->getCommune();
        $avenue=$adresse->getAvenue();
        $quartier=$adresse->getQuartier();
        $numero=$adresse->getNumero();

        
        $stmt=$this->bdConn->prepare("INSERT INTO t_adresse (ville,commune,avenue,quartier,numero) VALUES (:ville,:commune,:avenue,:quartier,:numero)");
        $stmt->bindParam(":ville",$ville);
        $stmt->bindParam(":commune",$commune);
        $stmt->bindParam(":avenue",$avenue);
        $stmt->bindParam(":quartier",$quartier);
        $stmt->bindParam(":numero",$numero);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function  findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_adresse WHERE Id_adresse=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;

    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_adresse ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function Delete(){

    } 
    public function DeleteById($id){

    }

    public function update($adresse){
      
	
    }
}

?>