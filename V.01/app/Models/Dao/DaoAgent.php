<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;


class DaoAgent implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($agent){
        $matricule=$agent->getMatriculeAgent();
        $phone=$agent->getPhone();
        $civilite=$agent->getCivilite();
        $niveau=$agent->getNiveauEtude();
        $idpersonne=$agent->getIdPersonne();
        $photo= $agent->getPhone();

       
        $stmt=$this->bdConn->prepare("INSERT INTO t_agent (matriculeAgent,phone,civilite,niveauEtude,Id_personne,photo) VALUES (:matricule, :phone,:civilite,:niveau,:idpersonne,:photo)");
        $stmt->bindParam(":matricule",$matricule);
        $stmt->bindParam(":phone",$phone);
        $stmt->bindParam(":civilite",$civilite);
        $stmt->bindParam(":niveau",$niveau);
        $stmt->bindParam(":idpersonne",$idpersonne);
        $stmt->bindParam(":photo",$photo);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){

    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_agent");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoPersonne=new DaoPersonne();
        $unDaoFonctionEmploie= new DaoFonctionEmploie();
        for ($i=0; $i < count($data); $i++) { 
 
          $datanew[$i]["matriculeAgent"]=$data[$i]["matriculeAgent"];
          $datanew[$i]["phone"]=$data[$i]["phone"];
          $datanew[$i]["civilite"]=$data[$i]["civilite"];
          $datanew[$i]["niveauEtude"]=$data[$i]["niveauEtude"];
          $datanew[$i]["personne"]=$unDaoPersonne->findOnebyId($data[$i]["Id_personne"]);
          $datanew[$i]["fonction"]=$unDaoFonctionEmploie->findOnebyId($data[$i]["matriculeAgent"]);
      }
      return $datanew;
    }
    public function findAllAgentByFonction($idfonction){
        $datanew=[];
        
        $stmt=$this->bdConn->prepare("SELECT*FROM t_agent INNER JOIN t_fonctionemploye ON t_fonctionemploye.matriculeAgent=t_agent.matriculeAgent WHERE t_fonctionemploye.Id_fonction=:id");
        $stmt->bindParam(":id",$idfonction);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoPersonne=new DaoPersonne();
        $unDaoFonctionEmploie= new DaoFonctionEmploie();
        for ($i=0; $i < count($data); $i++) { 
 
          $datanew[$i]["matriculeAgent"]=$data[$i]["matriculeAgent"];
          $datanew[$i]["phone"]=$data[$i]["phone"];
          $datanew[$i]["civilite"]=$data[$i]["civilite"];
          $datanew[$i]["niveauEtude"]=$data[$i]["niveauEtude"];
          $datanew[$i]["personne"]=$unDaoPersonne->findOnebyId($data[$i]["Id_personne"]);
          $datanew[$i]["fonction"]=$unDaoFonctionEmploie->findOnebyId($data[$i]["matriculeAgent"]);
      }
      return $datanew;
    }
    public function Count(){
      $stmt=$this->bdConn->prepare("SELECT count(matriculeAgent) as total FROM t_agent");
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      
      return $data;
     
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>