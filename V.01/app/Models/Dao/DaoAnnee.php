<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;

class DaoAnnee implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($annee){
        $libelle=$annee->getLibelleAnnee();
        $dateDebut=$annee->getDateDebut();
        $dateFin=$annee->getDateFin();
        $statut=$annee->getStatut();
        
        $stmt=$this->bdConn->prepare("INSERT INTO t_annee (libelleAnnee,dateDebut,dateFin,statut) VALUES (:libelle,:datedebut,:datefin,:statut)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":datedebut",$dateDebut);
        $stmt->bindParam(":datefin",$dateFin);
        $stmt->bindParam(":statut",$statut);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_annee WHERE Id_annee=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_annee ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAnneeOuverte(){
        $nbr=1;
        $stmt=$this->bdConn->prepare("SELECT*FROM t_annee WHERE statut=:nbr ");
        $stmt->bindParam(":nbr",$nbr);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
  }

?>