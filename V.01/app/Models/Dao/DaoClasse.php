<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Classe;

class DaoClasse implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($classe){
        $libelle=$classe->getLibelleClasse();
        $idcycle=$classe->getIdCycle();
        $idsection=$classe->getIdSection();
        $idoption=$classe->getIdOption();
        $iddivision=$classe->getIdDivision();

        
        $stmt=$this->bdConn->prepare("INSERT INTO t_classe (libelleClasse,Id_cycle,Id_section,Id_option,Id_division) VALUES (:libelle,:idcycles,:idsections,:idoptions,:iddevisions)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":idcycles",$idcycle);
        $stmt->bindParam(":idsections",$idsection);
        $stmt->bindParam(":idoptions",$idoption);
        $stmt->bindParam(":iddevisions",$iddivision);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }
    public function findOnebyId($id){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_classe WHERE Id_classe=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoSection = new DaoSection();
        $unDaoOption= new DaoOption();
        $unDaoCycle= new DaoCycle();
        $unDaoDivision= new DaoDivision();
        for ($i=0; $i < count($data); $i++) { 
 
            $datanew[$i]["Id_classe"]=$data[$i]["Id_classe"];
            $datanew[$i]["libelleClasse"]=$data[$i]["libelleClasse"];
            $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
            $datanew[$i]["section"]=$unDaoSection->findOnebyId($data[$i]["Id_section"]);
            $datanew[$i]["option"]=$unDaoOption->findOnebyId($data[$i]["Id_option"]);
            $datanew[$i]["division"]=$unDaoDivision->findOnebyId($data[$i]["Id_division"]);
           
        }
        return $datanew;
    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_classe ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoSection = new DaoSection();
        $unDaoOption= new DaoOption();
        $unDaoCycle= new DaoCycle();
        $unDaoDivision= new DaoDivision();
        for ($i=0; $i < count($data); $i++) { 
 
            $datanew[$i]["Id_classe"]=$data[$i]["Id_classe"];
            $datanew[$i]["libelleClasse"]=$data[$i]["libelleClasse"];
            $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
            $datanew[$i]["section"]=$unDaoSection->findOnebyId($data[$i]["Id_section"]);
            $datanew[$i]["option"]=$unDaoOption->findOnebyId($data[$i]["Id_option"]);
            $datanew[$i]["division"]=$unDaoDivision->findOnebyId($data[$i]["Id_division"]);
           
        }
        return $datanew;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>