<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Cours;

class DaoCours implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($cours){
        $libelle= $cours->getLibelleCours();
        $note= $cours->getNoteGeneral();
        $ponderation=$cours->getPonderation();
        $idCycle=$cours->getIdCycle();
        $idSection=$cours->getIdSection();
         $idOption=$cours->getIdOption();
    
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_cours (libelleCours,noteGeneral,ponderation,Id_cycle,Id_section, Id_option) VALUES (:libelle,:note,:ponde,:idcycle,:idsection,:idoption)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":note",$note);
        $stmt->bindParam(":ponde",$ponderation);
        $stmt->bindParam(":idcycle",$idCycle);
        $stmt->bindParam(":idsection",$idSection);
        $stmt->bindParam(":idoption",$idOption);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }
    public function findOnebyId($id){

    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_cours ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoSection = new DaoSection();
        $unDaoOption= new DaoOption();
        $unDaoCycle= new DaoCycle();
        for ($i=0; $i < count($data); $i++) { 
 
            $datanew[$i]["Id_cours"]=$data[$i]["Id_cours"];
            $datanew[$i]["libelleCours"]=$data[$i]["libelleCours"];
            $datanew[$i]["noteGeneral"]=$data[$i]["noteGeneral"];
            $datanew[$i]["ponderation"]=$data[$i]["ponderation"];
            $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
            $datanew[$i]["section"]=$unDaoSection->findOnebyId($data[$i]["Id_section"]);
            $datanew[$i]["option"]=$unDaoOption->findOnebyId($data[$i]["Id_option"]);
    
           
        }
        return $datanew;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>