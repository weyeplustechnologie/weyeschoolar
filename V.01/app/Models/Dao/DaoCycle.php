<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Cycle;

class DaoCycle implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($cycle){
        $libelle= $cycle->getLibelleCycle();
        $description= $cycle->getDescription();
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_cycle (libelleCycle,descriptionCycle) VALUES (:libelle,:descriptionCycle)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":descriptionCycle",$description);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_cycle WHERE Id_cycle=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_cycle ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>