<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Dispensecours;

class DaoDispensecours implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($dispensecours){
		
		$_matriculeAgent=$dispensecours->getMatriculeAgent();
		$_idCours=$dispensecours->getIdCours();
		$_idAnnee=$dispensecours->getIdAnnee();
		$_idClasse=$dispensecours->getIdClasse();
		  

    
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_dispensecours (matriculeAgent,Id_cours,Id_annee,Id_classe) VALUES (:matriculeAgent,:idCours,:idAnnee,:idClasse)");
        $stmt->bindParam(":matriculeAgent",$_matriculeAgent);
        $stmt->bindParam(":idCours",$_idCours);
        $stmt->bindParam(":idAnnee",$_idAnnee);
        $stmt->bindParam(":idClasse",$_idClasse);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){

    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_dispensecours ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>