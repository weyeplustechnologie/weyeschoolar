<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Division;

class DaoDivision implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($division){
        $libelle= $division->getLibelleDivision();
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_division (libelleDivision) VALUES (:libelle)");
        $stmt->bindParam(":libelle",$libelle);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_division WHERE Id_division=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_division ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>