<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Ecole;

class DaoEcole implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($ecole){

        $nomecole= $ecole->getNomEcole();
        $promoteur= $ecole->getPromteur();
        $devise=$ecole->getDevise();
        $type=$ecole->getTypeEcole();
        $logo=$ecole->getDevise();
        $idadresse=$ecole->getIdAdresse();
 
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_ecole (nom,promoteur,devise,typeEcole,logo,Id_adresse) VALUES (:nomecole,:promoteur,:devise,:typeecole,:logo,:idadresse)");
        $stmt->bindParam(":nomecole",$nomecole);
        $stmt->bindParam(":promoteur",$promoteur);
        $stmt->bindParam(":devise",$devise);
        $stmt->bindParam(":typeecole",$type);
        $stmt->bindParam(":logo",$logo);
        $stmt->bindParam(":idadresse",$idadresse);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function  findOnebyId($id){
 
    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_ecole ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoAdresse=new DaoAdresse();
        for ($i=0; $i < count($data); $i++) { 
            $datanew[0]["Id_ecole"]=$data[0]["Id_ecole"];
            $datanew[0]["nom"]=$data[0]["nom"];
            $datanew[0]["promoteur"]=$data[0]["promoteur"];
            $datanew[0]["devise"]=$data[0]["devise"];
            $datanew[0]["typeEcole"]=$data[0]["typeEcole"];
            $datanew[0]["logo"]=$data[0]["logo"];
            $datanew[0]["adresse"]=$unDaoAdresse->findOnebyId($data[0]["Id_adresse"]);
    
        }
      
      return $datanew;
    }
    
    public function Delete(){

    } 
    public function DeleteById($id){

    }

    public function update($ecole){
      
	
    }
}

?>