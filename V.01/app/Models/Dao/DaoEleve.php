<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Eleve;

class DaoEleve implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($eleve){
        $matricule=$eleve->getMatriculeEleve();
        $phone=$eleve->getPhoneTuteur();
        $tuteur=$eleve->getTuteur();
        $statut=$eleve->getStatutTuteur();
        $idpersonne=$eleve->getIdPersonne();
        $idannee=$eleve->getIdAnnee();

        
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_eleve (matriculeEleve,statutTuteur,tuteur,phoneTuteur,Id_personne, Id_annee) VALUES (:matricule,:statut,:tuteur,:phone,:idpersonne,:idannee)");
        $stmt->bindParam(":matricule",$matricule);
        $stmt->bindParam(":statut",$statut);
        $stmt->bindParam(":tuteur",$tuteur);
        $stmt->bindParam(":phone",$phone);
        $stmt->bindParam(":idpersonne",$idpersonne);
        $stmt->bindParam(":idannee",$idannee);

       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
      $datanew=[];
      $stmt=$this->bdConn->prepare("SELECT*FROM t_eleve WHERE matriculeEleve=:id");
      $stmt->bindParam(":id",$id);
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      $unDaoPersonne=new DaoPersonne();
      $unDaoAnnee= new DaoAnnee();
      for ($i=0; $i < count($data); $i++) { 
 
        $datanew[$i]["matriculeEleve"]=$data[$i]["matriculeEleve"];
        $datanew[$i]["statutTuteur"]=$data[$i]["statutTuteur"];
        $datanew[$i]["tuteur"]=$data[$i]["tuteur"];
        $datanew[$i]["phoneTuteur"]=$data[$i]["phoneTuteur"];
        $datanew[$i]["personne"]=$unDaoPersonne->findOnebyId($data[$i]["Id_personne"]);
        $datanew[$i]["annee"]=$unDaoAnnee->findOnebyId($data[$i]["Id_annee"]);
    }
    return $datanew;
    }
    public function findAll(){

    }
    public function DeleteById($id){

    }
    public function update($object){

    }
}

?>