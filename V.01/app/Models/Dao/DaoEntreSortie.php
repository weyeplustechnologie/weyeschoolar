<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\EntreSortie;

class DaoEntreSortie implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($entreSortie){
        $type=$entreSortie->getTypeOperation();
        $motif=$entreSortie->getMotif();
        $montant=$entreSortie->getMontant();
        $date=$entreSortie->getDateEntresortie();
        $concerne=$entreSortie->getConcerne();
        $iduser=$entreSortie->getIdUser();
        $idextension=$entreSortie->getIdExtension();
        $idannee=$entreSortie->getIdAnnee();
        $observation=$entreSortie->getObservation();

        $stmt=$this->bdConn->prepare("INSERT INTO t_entresortie (TypeOperation,motif,montant,observation,dateEntresortie,concerne,Id_user, Id_extension,Id_annee) VALUES (:typeoperation,:motif,:montant,:observation,:dateentre,:concerne,:iduser,:idextension,:idannee)");
        $stmt->bindParam(":typeoperation",$type);
        $stmt->bindParam(":motif",$motif);
        $stmt->bindParam(":montant",$montant);
        $stmt->bindParam(":dateentre",$date);
        $stmt->bindParam(":concerne",$concerne);
        $stmt->bindParam(":iduser",$iduser);
        $stmt->bindParam(":idextension",$idextension);
        $stmt->bindParam(":idannee",$idannee);
        $stmt->bindParam(":observation",$observation);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
      $stmt=$this->bdConn->prepare("SELECT*FROM t_frais WHERE Id_frais=:id");
      $stmt->bindParam(":id",$id);
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      return $data;
  }
  public function findAll(){
      $stmt=$this->bdConn->prepare("SELECT*FROM t_frais ");
      $stmt->execute();
      $datanew=[];
      $unDaoAnnee= new DaoAnnee();
      $unDaoTypefrais= new DaoTypeFrais();
      $unDaoCycle= new DaoCycle();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      for ($i=0; $i < count($data); $i++) { 
          $datanew[$i]["Id_frais"]=$data[$i]["Id_frais"];
          $datanew[$i]["montant"]=$data[$i]["montant"];
          $datanew[$i]["devise"]=$data[$i]["devise"];
          $datanew[$i]["annee"]= $unDaoAnnee->findOnebyId($data[$i]["Id_annee"]);
          $datanew[$i]["typefrais"]=$unDaoTypefrais->findOnebyId($data[$i]["Id_typefrais"]);
          $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
      }
      return $datanew;
  }
  public function DeleteById($id){

  }
  public function update($extension){

  }
}

?>