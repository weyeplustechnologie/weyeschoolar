<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;


class DaoExtension implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($extension){
        $libelle= $extension->getLibelleExtension();
        $description= $extension->getDescriptionExtension();
        $idecole=$extension->getIdEcole();
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_extension (libelleExtension,descriptionExtension,Id_ecole) VALUES (:libelle,:descriptionextension,:idecole)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":descriptionextension",$description);
        $stmt->bindParam(":idecole",$idecole);

       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }
    public function  findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_extension WHERE Id_extension=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_extension ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function Delete(){

    } 
    public function DeleteById($id){

    }

    public function update($ecole){
      
	
    }
}
	  
?>