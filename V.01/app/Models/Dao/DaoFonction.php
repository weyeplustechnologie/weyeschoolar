<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Fonction;

class DaoFonction implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($fonction){
        $libelle= $fonction->getLibelleFonction();
        $description= $fonction->getDescriptionFonction();
        
        $stmt=$this->bdConn->prepare("INSERT INTO t_fonction (libelleFonction,descriptionFonction) VALUES (:libelleFonction,:descriptionFonction)");
        $stmt->bindParam(":libelleFonction",$libelle);
        $stmt->bindParam(":descriptionFonction",$description);

       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_fonction WHERE id_fonction=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_fonction");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>