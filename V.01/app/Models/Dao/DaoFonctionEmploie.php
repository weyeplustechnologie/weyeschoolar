<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\FonctionEmploie;

class DaoFonctionEmploie implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($fonctionEmploie){
        $matricule=$fonctionEmploie->getMatriculeAgent();
        $idfonction=$fonctionEmploie->getIdFonction();
        $idannee=$fonctionEmploie->getIdAnnee();
        

        $stmt=$this->bdConn->prepare("INSERT INTO t_fonctionemploye (matriculeAgent,Id_fonction,Id_annee) VALUES (:matricule,:idfonction,:idannee)");
        $stmt->bindParam(":matricule",$matricule);
        $stmt->bindParam(":idfonction",$idfonction);
        $stmt->bindParam(":idannee",$idannee);
     
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }
    public function  findOnebyId($id){
      $datanew=[];
      $stmt=$this->bdConn->prepare("SELECT*FROM t_fonctionemploye WHERE matriculeAgent=:id");
      $stmt->bindParam(":id",$id);
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      $unDaoAnnee= new DaoAnnee();
      $UnDaoFonction= new DaoFonction();
          $datanew[0]["Id_fonctionEmploye"]=$data[0]["Id_fonctionEmploye"];
          $datanew[0]["matriculeAgent"]=$data[0]["matriculeAgent"];
          $datanew[0]["fonction"]=$UnDaoFonction->findOnebyId($data[0]["Id_fonction"]);;
          $datanew[0]["annee"]=$unDaoAnnee->findOnebyId($data[0]["Id_annee"]);
          
          return $datanew;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_fonctionemploye ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function Delete(){

    } 
    public function DeleteById($id){

    }

    public function update($ecole){
      
	
    }
	   
}

?>