<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Frais;

class DaoFrais implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($frais){
        $montant= $frais->getMontant();
        $devise= $frais->getDevise();
        $idannee= $frais->getIdAnnee();
        $idtypefrais= $frais->getIdTypeFrais();
        $idcycle=$frais->getIdCycle();

        $stmt=$this->bdConn->prepare("INSERT INTO t_frais (montant,devise,Id_annee,Id_typefrais, Id_cycle) VALUES (:montant,:devise,:idannee,:idtypefrais, :idcycle)");
        $stmt->bindParam(":montant",$montant);
        $stmt->bindParam(":devise",$devise);
        $stmt->bindParam(":idannee",$idannee);
        $stmt->bindParam(":idtypefrais",$idtypefrais);
        $stmt->bindParam(":idcycle",$idcycle);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_frais WHERE Id_frais=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_frais ");
        $stmt->execute();
        $datanew=[];
        $unDaoAnnee= new DaoAnnee();
        $unDaoTypefrais= new DaoTypeFrais();
        $unDaoCycle= new DaoCycle();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        for ($i=0; $i < count($data); $i++) { 
            $datanew[$i]["Id_frais"]=$data[$i]["Id_frais"];
            $datanew[$i]["montant"]=$data[$i]["montant"];
            $datanew[$i]["devise"]=$data[$i]["devise"];
            $datanew[$i]["annee"]= $unDaoAnnee->findOnebyId($data[$i]["Id_annee"]);
            $datanew[$i]["typefrais"]=$unDaoTypefrais->findOnebyId($data[$i]["Id_typefrais"]);
            $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
        }
        return $datanew;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>