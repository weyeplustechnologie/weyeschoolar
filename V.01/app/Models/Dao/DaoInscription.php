<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Inscription;

class DaoInscription implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($inscription){
        $matricule=$inscription->getMatriculeEleve();
        $idclasse=$inscription->getIdClasse();
        $idannee=$inscription->getIdAnnee();
        $idcycle=$inscription->getIdCycle();
        $idoption=$inscription->getIdOption();
        $idsection=$inscription->getIdSection();
        $idextension=$inscription->getIdExtension();
        $statut=$inscription->getStatutInscription();
        

        $stmt=$this->bdConn->prepare("INSERT INTO t_inscription (matriculeEleve,Id_annee,Id_cycle, Id_section, Id_option,Id_classe,Id_extension,statutInscription) VALUES (:matricule,:idannee,:idcycle,:idsection,:idoption,:idclasse,:idextension,:statut)");
        $stmt->bindParam(":matricule",$matricule);
        $stmt->bindParam(":idannee",$idannee);
        $stmt->bindParam(":idclasse",$idclasse);
        $stmt->bindParam(":idextension",$idextension);
        $stmt->bindParam(":idcycle",$idcycle);
        $stmt->bindParam(":idoption",$idoption);
        $stmt->bindParam(":idsection",$idsection);
        $stmt->bindParam(":statut",$statut);
     
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }
    public function findOnebyId($id){

    }
    public function findAll(){

    }
    public function findAllActive(){
      $datanew=[];
      $statut=1;
      $stmt=$this->bdConn->prepare("SELECT*FROM t_inscription WHERE statutInscription=:statut");
      $stmt->bindParam(":statut",$statut);
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      $unDaoEleve=new DaoEleve();
      $unDaoAnnee= new DaoAnnee();
      $unDaoCycle= new DaoCycle();
      $unDaoClasse= new DaoClasse();
      $unDaoSection= new DaoSection();
      $unDaoOption= new DaoOption();
      $unDaoExtension= new DaoExtension();

      for ($i=0; $i < count($data); $i++) { 
        $datanew[$i]["Id_inscription"]=$data[$i]["Id_inscription"];
        $datanew[$i]["eleve"]=$unDaoEleve->findOnebyId($data[$i]["matriculeEleve"]);
        $datanew[$i]["annee"]=$unDaoAnnee->findOnebyId($data[$i]["Id_annee"]);
        $datanew[$i]["section"]=$unDaoSection->findOnebyId($data[$i]["Id_section"]);
        $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
        $datanew[$i]["option"]=$unDaoOption->findOnebyId($data[$i]["Id_option"]);
        $datanew[$i]["classe"]=$unDaoClasse->findOnebyId($data[$i]["Id_classe"]);
        $datanew[$i]["extension"]=$unDaoExtension->findOnebyId($data[$i]["Id_extension"]);
        $datanew[$i]["statutInscription"]=$data[$i]["statutInscription"];
    }
    return $datanew;
    }
    public function findAllNonActive(){

    }
    public function Count(){
      $stmt=$this->bdConn->prepare("SELECT count(matriculeEleve) as total FROM t_inscription");
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      
      return $data;
     
    }
    public function DeleteById($id){

    }
    public function update($object){
      
    }
	   
}

?>