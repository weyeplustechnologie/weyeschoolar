<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Notes;

class DaoNotes implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create(Notes $notes){
		$_idCours=$notes->getIdCours();
		$_idClasse=$notes->getIdClasse();
		$_idTypeSession=$notes->getIdTypeSession();
		$_matriculeEleve=$notes->getMatriculeEleve();
		$_notesObtenu=$notes->getNotesObtenu();
    
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_notes (Id_cours,Id_classe,Id_typeSession,matriculeEleve,notesObtenu) VALUES (:idCours,:idClasse,:idTypeSession,:matriculeEleve,:notesObtenu)");
        $stmt->bindParam(":idCours",$_idCours);
        $stmt->bindParam(":idClasse",$_idClasse);
        $stmt->bindParam(":idTypeSession",$_idTypeSession);
		$stmt->bindParam(":matriculeEleve",$_matriculeEleve);
        $stmt->bindParam(":notesObtenu",$_notesObtenu);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

	    public function  findOnebyId($id){
	
		
		
		$id = (int) $id;
		$pp = $this->bdConn->query('SELECT * FROM t_notes WHERE id = '.$id);
		$donnees = $pp->fetch(PDO::FETCH_ASSOC);
		return new Notes($donnees);
 
    }
    public function findAll(){
	
	
		$notes = array();
		$pp = $this->bdConn->query('SELECT * FROM t_notes');
		while ($donnees = $pp->fetch(PDO::FETCH_ASSOC))
		{
			$notes[] = new Notes($donnees);
		}
		return $notes;
     
    }
    public function Delete(){
		$this->bdConn->exec('DELETE FROM t_notes');
    } 
    public function DeleteById($id){
	
		$id = (int) $id;
		$this->bdConn->exec('DELETE FROM t_notes WHERE id = '.$id);

    }

    public function update(Notes $notes){
		$id=(int)$notes->getIdNotes();
		$_idCours=$notes->getIdCours();
		$_idClasse=$notes->getIdClasse();
		$_idTypeSession=$notes->getIdTypeSession();
		$_matriculeEleve=$notes->getMatriculeEleve();
		$_notesObtenu=$notes->getNotesObtenu();
    
       $stmt=$this->bdConn->prepare('UPDATE t_notes SET Id_cours=:idCours,Id_classe=:idClasse,Id_typeSession=:idTypeSession,matriculeEleve=:matriculeEleve,notesObtenu=:notesObtenu WHERE id=:id');
       
        //$stmt=$this->bdConn->prepare("INSERT INTO t_notes (Id_cours,Id_classe,Id_typeSession,matriculeEleve,notesObtenu) VALUES (:idCours,:idClasse,:idTypeSession,:matriculeEleve,:notesObtenu)");
        $stmt->bindParam(":idCours",$_idCours);
        $stmt->bindParam(":idClasse",$_idClasse);
        $stmt->bindParam(":idTypeSession",$_idTypeSession);
		$stmt->bindParam(":matriculeEleve",$_matriculeEleve);
        $stmt->bindParam(":notesObtenu",$_notesObtenu);
		$stmt->bindParam(":id",$id);
        $stmt->execute();
      
	
    }
}

?>