<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Option;

class DaoOption implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($option){
        $libelle= $option->getLibelleOption();
        $description= $option->getDescriptionOption();
        $idsection=$option->getIdSection();
       
        $stmt=$this->bdConn->prepare("INSERT INTO t_option (libelleOption,descriptionOption,Id_section) VALUES (:libelle,:descriptionsection,:idsections)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":descriptionsection",$description);
        $stmt->bindParam(":idsections",$idsection);
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_option WHERE Id_option=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_option ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoSection = new DaoSection();
        for ($i=0; $i < count($data); $i++) { 
 
            $datanew[$i]["Id_option"]=$data[$i]["Id_option"];
            $datanew[$i]["libelleOption"]=$data[$i]["libelleOption"];
            $datanew[$i]["descriptionOption"]=$data[$i]["descriptionOption"];
            $datanew[$i]["section"]=$unDaoSection->findOnebyId($data[$i]["Id_section"]);
           
        }
        return $datanew;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>