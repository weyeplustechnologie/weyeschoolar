<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\PaiementFrais;

class DaoPaiementFrais implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($paiementFrais){
        $matricule=$paiementFrais->getMatriculeEleve();
        $montant=$paiementFrais->getMontant();
        $idannee=$paiementFrais->getIdAnnee();
        $idtranche=$paiementFrais->getIdPaiementtranche();
        $typefrais=$paiementFrais->getIdTypefrais();
        $iduser=$paiementFrais->getIdUser();
        $idcycle=$paiementFrais->getIdCycle();
        $idextension=$paiementFrais->getIdExtension();
        $devise=$paiementFrais->getDevise();
        $date=$paiementFrais->getDatePaiement();
        
        
        $stmt=$this->bdConn->prepare("INSERT INTO  t_paiementfrais (montantPaie,devise,datePaiement,Id_annee,Id_paiementtranche,Id_typefrais,matriculeEleve,Id_user,Id_cycle, Id_extension) VALUES (:montant,:devise,:datepaie,:idannee,:idtranche,:idtypefrais,:matricule,:iduser,:idcycle,:idextension)");
        $stmt->bindParam(":montant",$montant);
        $stmt->bindParam(":devise",$devise);
        $stmt->bindParam(":datepaie",$date);
        $stmt->bindParam(":idannee",$idannee);
        $stmt->bindParam(":idtranche",$idtranche);
        $stmt->bindParam(":idcycle",$idcycle);
        $stmt->bindParam(":idextension",$idextension);
        $stmt->bindParam(":idtypefrais",$typefrais);
        $stmt->bindParam(":matricule",$matricule);
        $stmt->bindParam(":iduser",$iduser);
     
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
      $stmt=$this->bdConn->prepare("SELECT*FROM t_frais WHERE Id_frais=:id");
      $stmt->bindParam(":id",$id);
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      return $data;
  }
  public function findAll(){
      $stmt=$this->bdConn->prepare("SELECT*FROM t_frais ");
      $stmt->execute();
      $datanew=[];
      $unDaoAnnee= new DaoAnnee();
      $unDaoTypefrais= new DaoTypeFrais();
      $unDaoCycle= new DaoCycle();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      for ($i=0; $i < count($data); $i++) { 
          $datanew[$i]["Id_frais"]=$data[$i]["Id_frais"];
          $datanew[$i]["montant"]=$data[$i]["montant"];
          $datanew[$i]["devise"]=$data[$i]["devise"];
          $datanew[$i]["annee"]= $unDaoAnnee->findOnebyId($data[$i]["Id_annee"]);
          $datanew[$i]["typefrais"]=$unDaoTypefrais->findOnebyId($data[$i]["Id_typefrais"]);
          $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
      }
      return $datanew;
  }
  public function DeleteById($id){

  }
  public function update($extension){

  }
	   
}

?>