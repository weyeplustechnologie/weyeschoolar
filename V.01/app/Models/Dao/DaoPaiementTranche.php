<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\PaiementTranche;

class DaoPaiementTranche implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($paiementTranche){
        
        $montant=$paiementTranche->getMontant();
        $devise=$paiementTranche->getDevise();
        $idannee=$paiementTranche->getIdAnnee();
        $idtranche=$paiementTranche->getIdTranche();
        $idtypefrais=$paiementTranche->getIdTypefrais();
        $idcycle=$paiementTranche->getIdCycle();
       
        
        $stmt=$this->bdConn->prepare("INSERT INTO t_paiementtranche (montant,devise,Id_annee,Id_tranche,Id_typefrais,Id_cycle) VALUES (:montant,:devise,:idannee,:idtranche,:idtypefrais,:idcycle)");
        $stmt->bindParam(":montant",$montant);
        $stmt->bindParam(":devise",$devise);
        $stmt->bindParam(":idannee",$idannee);
        $stmt->bindParam(":idtranche",$idtranche);
        $stmt->bindParam(":idtypefrais",$idtypefrais);
        $stmt->bindParam(":idcycle",$idcycle);
     
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }
    public function findOnebyId($id){
      $stmt=$this->bdConn->prepare("SELECT*FROM t_paiementtranche WHERE Id_frais=:id");
      $stmt->bindParam(":id",$id);
      $stmt->execute();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      return $data;
  }
  public function findAll(){
      $stmt=$this->bdConn->prepare("SELECT*FROM t_paiementtranche ");
      $stmt->execute();
      $datanew=[];
      $unDaoAnnee= new DaoAnnee();
      $unDaoTypefrais= new DaoTypeFrais();
      $unDaoCycle= new DaoCycle();
      $unDaoTranche= new DaoTranche();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      for ($i=0; $i < count($data); $i++) { 
          $datanew[$i]["Id_paiementTranche"]=$data[$i]["Id_paiementTranche"];
          $datanew[$i]["montant"]=$data[$i]["montant"];
          $datanew[$i]["devise"]=$data[$i]["devise"];
          $datanew[$i]["annee"]= $unDaoAnnee->findOnebyId($data[$i]["Id_annee"]);
          $datanew[$i]["typetranche"]=$unDaoTranche->findOnebyId($data[$i]["Id_tranche"]);
          $datanew[$i]["typefrais"]=$unDaoTypefrais->findOnebyId($data[$i]["Id_typefrais"]);
          $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
      }
      return $datanew;
  }
  public function DeleteById($id){

  }
  public function update($extension){

  }

	   
}

?>