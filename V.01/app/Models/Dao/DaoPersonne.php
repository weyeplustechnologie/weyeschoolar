<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Personne;

class DaoPersonne implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($personne){
        
        $nom=$personne->getNom();
        $postnom=$personne->getPostnom();
        $prenom=$personne->getPrenom();
        $sexe=$personne->getSexe();
        $lieuNaissance=$personne->getLieuNaissance();
        $dateNaissance=$personne->getDateNaissance();
        $province=$personne->getProvince();
        $nationalite=$personne->getNationalite();
        $idAdresse=$personne->getIdAdresse();
       
        
        $stmt=$this->bdConn->prepare("INSERT INTO t_personne (nom,postnom,prenom,sexe,lieuNaissance,dateNaissance,province,nationalite,Id_adresse) VALUES (:nom,:postnom,:prenom,:sexe,:lieu,:datenai,:province,:nationalite,:idadresse)");
        $stmt->bindParam(":nom",$nom);
        $stmt->bindParam(":postnom",$postnom);
        $stmt->bindParam(":prenom",$prenom);
        $stmt->bindParam(":sexe",$sexe);
        $stmt->bindParam(":lieu",$lieuNaissance);
        $stmt->bindParam(":datenai",$dateNaissance);
        $stmt->bindParam(":province",$province);
        $stmt->bindParam(":nationalite",$nationalite);
        $stmt->bindParam(":idadresse",$idAdresse);
     
       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){
      $datanew=[];
      $stmt=$this->bdConn->prepare("SELECT*FROM t_personne WHERE Id_personne=:id");
      $stmt->bindParam(":id",$id);
      $stmt->execute();
      $unDaoAdresse=new DaoAdresse();
      $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
      
      $datanew[0]["nom"]=$data[0]["nom"];
      $datanew[0]["postnom"]=$data[0]["postnom"];
      $datanew[0]["prenom"]=$data[0]["prenom"];
      $datanew[0]["sexe"]=$data[0]["sexe"];
      $datanew[0]["lieuNaissance"]=$data[0]["lieuNaissance"];
      $datanew[0]["dateNaissance"]=$data[0]["dateNaissance"];
      $datanew[0]["province"]=$data[0]["province"];
      $datanew[0]["nationalite"]=$data[0]["nationalite"];
      $datanew[0]["adresse"]=$unDaoAdresse->findOnebyId($data[0]["Id_adresse"]);
      return $datanew;
    }
    public function findAll(){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_personne ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function Delete(){

    } 
    public function DeleteById($id){

    }

    public function update($personne){
      
	
    }
}

?>