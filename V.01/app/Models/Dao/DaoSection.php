<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;


class DaoSection implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($section){
        $libelle= $section->getLibelleSection();
        $description= $section->getDescriptionSection();
        $idcycle=$section->getIdCyle();

        $stmt=$this->bdConn->prepare("INSERT INTO t_section (libelleSection,descriptionSection,Id_cycle) VALUES (:libelle,:descriptionsection,:idcycles)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":descriptionsection",$description);
        $stmt->bindParam(":idcycles",$idcycle);
        if($stmt->execute()) {
          return $this->bdConn->lastInsertId();
      }
      else{
          return null;
      }
      
        
    }

    public function findOnebyId($id){
        $stmt=$this->bdConn->prepare("SELECT*FROM t_section WHERE Id_section=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_section ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoCycle=new DaoCycle();
        for ($i=0; $i < count($data); $i++) { 
 
            $datanew[$i]["Id_section"]=$data[$i]["Id_section"];
            $datanew[$i]["libelleSection"]=$data[$i]["libelleSection"];
            $datanew[$i]["descriptionSection"]=$data[$i]["descriptionSection"];
            $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
           
        }
        return $datanew;
     
    }
    public function DeleteById($id){

    }
    public function update($section){

    }
}

?>