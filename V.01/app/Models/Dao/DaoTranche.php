<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;
use App\Models\Entity\Tranche;

class DaoTranche implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($tranche){
        $libelle= $tranche->getLibelleTranche();
        $idtypefrais= $tranche->getIdTypeFrais();

        $stmt=$this->bdConn->prepare("INSERT INTO t_tranche (libelleTranche,Id_typefrais) VALUES (:libelle,:typefrais)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":typefrais",$idtypefrais);

       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }
    public function findOnebyId($id){
     
        $stmt=$this->bdConn->prepare("SELECT*FROM t_tranche WHERE Id_tranche=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
       
    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_tranche");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoTypefrais= new DaoTypeFrais();
        for ($i=0; $i < count($data); $i++) { 
            $datanew[$i]["Id_tranche"]=$data[$i]["Id_tranche"];
            $datanew[$i]["libelleTranche"]=$data[$i]["libelleTranche"];
            $datanew[$i]["typefrais"]=$unDaoTypefrais->findOnebyId($data[$i]["Id_typefrais"]);
        }
        return $datanew;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>