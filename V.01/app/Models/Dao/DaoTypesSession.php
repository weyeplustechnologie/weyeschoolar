<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;


class DaoTypesSession implements iDAO
{
    private $bdConn;

    function __construct()
    {
        $bd = new BdConnect();
        $this->bdConn= $bd->connect();
        
    }
    public function create($typeSession){
        $libelle= $typeSession->getLibelleTypeSession();
        $idCycle=$typeSession->getIdCycle();


        $stmt=$this->bdConn->prepare("INSERT INTO t_typesession (Id_cycle,libelletypeSession) VALUES (:idCycles,:libelle)");
        $stmt->bindParam(":libelle",$libelle);
        $stmt->bindParam(":idCycles",$idCycle);

       if($stmt->execute()) {
            return $this->bdConn->lastInsertId();
        }
        else{
            return null;
        }
        
    }

    public function findOnebyId($id){

    }
    public function findAll(){
        $datanew=[];
        $stmt=$this->bdConn->prepare("SELECT*FROM t_typesession ");
        $stmt->execute();
        $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $unDaoCycle= new DaoCycle();
        for ($i=0; $i < count($data); $i++) { 
 
            $datanew[$i]["Id_typeSession"]=$data[$i]["Id_typeSession"];
            $datanew[$i]["libelletypeSession"]=$data[$i]["libelletypeSession"];
            $datanew[$i]["cycle"]=$unDaoCycle->findOnebyId($data[$i]["Id_cycle"]);
        
           
        }
        return $datanew;
    }
    public function DeleteById($id){

    }
    public function update($extension){

    }
}

?>