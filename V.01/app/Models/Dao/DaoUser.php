<?php

namespace App\Models\Dao;
use App\Models\Dao\iDAO;
use App\Models\BdConnect;


    class DaoUser  implements iDAO
    {
        private $bdConn;

        public function __construct(){

            $bd = new BdConnect();
           $this->bdConn= $bd->connect();
        }
        public function create($user){
       
            $identifiant= $user->getIdentifiant();
            $password= $user->getPassword();
            $matricule=$user->getMatricule();
            $statut=$user->getStatut();
            $role=$user->getRole();
            $created=$user->getCreateBy();
            $datecreated=$user->getCreateOn();
            $idExtension=$user->getIdExtension();
           
           
            $stmt=$this->bdConn->prepare("INSERT INTO t_users (login,password,matriculeAgent,statut,role,created_by,createdDate,id_extension) VALUES ( :login,:password,:matricule,:statut,:role,:created,:datecreated,:idextension)");
            $stmt->bindParam(":login",$identifiant);
            $stmt->bindParam(":password",$password);
            $stmt->bindParam(":matricule",$matricule);
            $stmt->bindParam(":statut",$statut);
            $stmt->bindParam(":role",$role);
            $stmt->bindParam(":created",$created);
            $stmt->bindParam(":datecreated",$datecreated);
            $stmt->bindParam(":idextension",$idExtension);
           if($stmt->execute()) {
                return $this->bdConn->lastInsertId();
            }
            else{
                return null;
            }
        }
        public function findOnebyId($id){
            $stmt=$this->bdConn->prepare("SELECT*FROM t_users WHERE Id_users=:id");
            $stmt->bindParam(":id",$id);
            $stmt->execute();
            $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
        }
        public function checkUser($user){
            $identifiant= $user->getIdentifiant();
            $password= $user->getPassword();
            $stmt=$this->bdConn->prepare("SELECT*FROM t_users WHERE  login=:login AND password=:password");
            $stmt->bindParam(":login",$identifiant);
            $stmt->bindParam(":password",$password);
            $stmt->execute();
            $data= $stmt->fetch(\PDO::FETCH_ASSOC);
            return $data;
        }
        public function findAll(){
            $datanew=[];
            $stmt=$this->bdConn->prepare("SELECT*FROM t_users ");
            $stmt->execute();
            $data= $stmt->fetchAll(\PDO::FETCH_ASSOC);
           $unDaoExtension=new DaoExtension();
           $unDaoUser= new DaoUser();
            for ($i=0; $i < count($data); $i++) { 
             $datanew[$i]["Id_user"]=$data[$i]["Id_user"];
             $datanew[$i]["login"]=$data[$i]["login"];
             $datanew[$i]["password"]=$data[$i]["password"];
                $datanew[$i]["matriculeAgent"]=$data[$i]["matriculeAgent"];
                $datanew[$i]["statut"]=$data[$i]["statut"];
                $datanew[$i]["role"]=$data[$i]["role"];
                $datanew[$i]["created_by"]="1";
                $datanew[$i]["createdDate"]=$data[$i]["createdDate"];
                $datanew[$i]["id_extension"]=$unDaoExtension->findOnebyId($data[$i]["id_extension"]);
         
            }
            return $datanew;
        }
        public function DeleteById($id){

        }
        public function update($extension){

        }
   
    }
    
?>