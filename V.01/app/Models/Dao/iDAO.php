<?php
namespace App\Models\Dao;
    interface  iDAO {
        public function create($object);
        public function findOnebyId($id);
        public function findAll();
        public function DeleteById($id); 
        public function update($object);
    }
?>