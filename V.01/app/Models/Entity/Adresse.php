<?php 
namespace App\Models\Entity;

class Adresse
{
    private $_idAdresse	;
    private $_ville	;
    private $_commune;
    private $_avenue;	
    private $_quartier;	
    private $_numero;

  

    function __construct()
    {
        
    }
    
    function getIdAdresse(){
        return $this->_idAdresse;
    }
    function setIdAdresse($id){
        $this->_idAdresse= $id;
    }
    function getVille(){
        return $this->_ville;
    }
    function setVille($ville){
        $this->_ville = $ville;
    }
    function getCommune(){
        return $this->_commune;
    }
    function setCommune($commune){
        $this->_commune = $commune;
    }
    function getAvenue(){
        return $this->_avenue;
    }
    function setAvenue($avenue){
        $this->_avenue = $avenue;
    }
    function getQuartier(){
        return $this->_quartier;
    }
    function setQuartier($quartier){
        $this->_quartier = $quartier;
    }
    function getNumero(){
        return $this->_numero;
    }
    function setNumero($numero){
        $this->_numero = $numero;
    }

}


?>