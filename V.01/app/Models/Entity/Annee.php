<?php 
namespace App\Models\Entity;

class Annee
{
    private  $_idAnnee;
    private $_libelleAnnee;
    private $_dateDebut;
    private $_dateFin;
    private $_statut;
  

    function __construct()
    {
        
    }
    
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }
    function getLibelleAnnee(){
        return $this->_libelleAnnee;
    }
    function setLibelleAnnee($libelle){
        $this->_libelleAnnee = $libelle;
    }
    function getDateDebut(){
        return $this->_dateDebut;
    }
    function setDateDebut($datedebut){
        $this->_dateDebut = $datedebut;
    }
    function getDateFin(){
        return $this->_dateFin;
    }
    function setDateFin($datefin){
        $this->_dateFin = $datefin;
    }
    function getStatut(){
        return $this->_statut;
    }
    function setStatut($statut){
        $this->_statut = $statut;
    }
  

}


?>