<?php 
namespace App\Models\Entity;

class Classe
{
    private  $_idClasse;
    private $_libelleClasse;
    private $_idCycle;
    private $_idSection;
    private $_idOption;
    private $_idDivision;
  

    function __construct()
    {
        
    }
    
    function getIdClasse(){
        return $this->_idClasse;
    }
    function setIdClasse($id){
        $this->_idClasse = $id;
    }
    function getLibelleClasse(){
        return $this->_libelleClasse;
    }
    function setLibelleClasse($libelle){
        $this->_libelleClasse = $libelle;
    }
    function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($id){
        $this->_idCycle = $id;
    }
    function getIdSection(){
        return $this->_idSection;
    }
    function setIdSection($id){
        $this->_idSection = $id;
    }
    function getIdOption(){
        return $this->_idOption;
    }
    function setIdOption($id){
        $this->_idOption = $id;
    }
    function getIdDivision(){
        return $this->_idDivision;
    }
    function setIdDivision($id){
        $this->_idDivision = $id;
    }

}


?>