<?php 
namespace App\Models\Entity;

class Cours
{
    private  $_idCours;
    private $_libelleCours;
    private $_noteGeneral;
    private $_ponderation;
    private  $_idCycle;
    private  $_idSection;
    private  $_idOption;

    function __construct()
    {
        
    }
    
    function getIdCours(){
        return $this->_idCours;
    }
    function setIdCours($id){
        $this->_idCours = $id;
    }
  
    function getLibelleCours(){
        return $this->_libelleCours;
    }
    function setLibelleCours($libelle){
        $this->_libelleCours = $libelle;
    }
    function getNoteGeneral(){
        return $this->_noteGeneral;
    }
    function setNoteGeneral($noteGeneral){
        $this->_noteGeneral = $noteGeneral;
    }
    function getPonderation(){
        return $this->_ponderation;
    }
    function setPonderation($nbr){
        $this->_ponderation = $nbr;
    }
    function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($id){
        $this->_idCycle = $id;
    }
    function getIdSection(){
        return $this->_idSection;
    }
    function setIdSection($id){
        $this->_idSection = $id;
    }
    function getIdOption(){
        return $this->_idOption;
    }
    function setIdOption($id){
        $this->_idOption = $id;
    }
  
 
}


?>