<?php 
namespace App\Models\Entity;

class Cycle
{
    private  $_idCycle;
    private $_libelleCycle;
    private $_descriptionCycle;
 

  

    function __construct()
    {
        
    }
    
    function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($id){
        $this->_idCycle = $id;
    }
    function getLibelleCycle(){
        return $this->_libelleCycle;
    }
    function setLibelleCycle($libelle){
        $this->_libelleCycle = $libelle;
    }
    function getDescription(){
        return $this->_descriptionCycle;
    }
    function setDescription($description){
        $this->_descriptionCycle = $description;
    }

  

}


?>