<?php 
namespace App\Models\Entity;

class DemandeCoin
{
    private  $_phone;
    private $_adresse_wallet;
    private $_montantpaye;
    private $_coinpaie;
    private $_ref_transaction;
    private $_statut;

    function __construct()
    {
        
    }
    
    function getPhone(){
        return $this->_phone;
    }
    function setPhone($phone){
        $this->_phone = $phone;
    }
    function getAdresseWallet(){
        return $this->_adresse_wallet;
    }
    function setAdresseWallet($adresse_wallet ){
        $this->_adresse_wallet = $adresse_wallet;
    }
    function getMontantPaye(){
        return $this->_montantpaye;
    }
    function setMontantPaye($montantpaye){
        $this->_montantpaye = $montantpaye;
    }
    function getCoinPaie(){
        return $this->_coinpaie;
    }
    function setCoinPaie($coinpaie){
        $this->_coinpaie = $coinpaie;
    }
    function getRefTransaction(){
        return $this->_ref_transaction;
    }
    function setRefTransaction($ref_transaction){
        $this->_ref_transaction = $ref_transaction;
    }
    function getStatut(){
        return $this->_statut;
    }
    function setStatut($_statut){
        $this->_statut = $_statut;
    }

}


?>