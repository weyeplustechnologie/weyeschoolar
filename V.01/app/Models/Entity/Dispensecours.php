<?php 
namespace App\Models\Entity;

class Dispensecours
{

  private  $_idDispenseCours;
  private  $_matriculeAgent;
  private  $_idCours;
  private  $_idAnnee;
  private  $_idCycle;
  private  $_idClasse;
  
  
    function __construct()
    {
        
    }
    
    function getIdDispenseCours(){
        return $this->_idDispenseCours;
    }
    function setIdDispenseCours($id){
        $this->_idDispenseCours = $id;
    }
	
	function getMatriculeAgent(){
        return $this->_matriculeAgent;
    }
    function setMatriculeAgent($matriculeAgent){
        $this->_matriculeAgent = $matriculeAgent;
    }
	
	function getIdCours(){
        return $this->_idCours;
    }
    function setIdCours($idcours){
        $this->_idCours = $idcours;
    }
	
	function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($idannee){
        $this->_idAnnee = $idannee;
    }
	
	function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($idcycle){
        $this->_idCycle = $idcycle;
    }
	
	function getIdClasse(){
        return $this->_idClasse;
    }
    function setIdClasse($idclasse){
        $this->_idClasse = $idclasse;
    }

}

?>