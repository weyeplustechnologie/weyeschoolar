<?php 
namespace App\Models\Entity;

class Division
{
    private  $_idDivision;
    private $_libelleDivision;


    function __construct()
    {
        
    }
    
    function getIdDivision(){
        return $this->_idDivision;
    }
    function setIdDivision($id){
        $this->_idDivision = $id;
    }
    function getLibelleDivision(){
        return $this->_libelleDivision;
    }
    function setLibelleDivision($libelle){
        $this->_libelleDivision = $libelle;
    }
    

  

}


?>