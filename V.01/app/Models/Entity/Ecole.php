<?php 
namespace App\Models\Entity;

class Ecole
{
    private  $_idEcole;
    private $_nomEcole;
    private $_promoteur;
    private $_devise;
    private $_typeEcole;
    private $_logo;
    private $_idAdresse;
    private $_codeEcole;
    

    function __construct()
    {
        
    }
    
    function getIdEcole(){
        return $this->_idEcole;
    }
    function setIdEcole($id){
        $this->_idEcole = $id;
    }
  
    function getNomEcole(){
        return $this->_nomEcole;
    }
    function setNomEcole($nom){
        $this->_nomEcole = $nom;
    }
    function getPromteur(){
        return $this->_promoteur;
    }
    function setPromteur($promoteur){
        $this->_promoteur = $promoteur;
    }
    function getDevise(){
        return $this->_devise;
    }
    function setDevise($devise){
        $this->_devise = $devise;
    }
    function getTypeEcole(){
        return $this->_typeEcole;
    }
    function setTypeEcole($type){
        $this->_typeEcole = $type;
    }
    function getLogo(){
        return $this->_logo;
    }
    function setLogo($logo){
        $this->_logo = $logo;
    }
    function getIdAdresse(){
        return $this->_idAdresse;
    }
    function setIdAdresse($idadresse){
        $this->_idAdresse = $idadresse;
    }
    function getCodeEcole(){
        return $this->_codeEcole;
    }
    function setCodeEcole($code){
        $this->_codeEcole = $code;
    }

}


?>