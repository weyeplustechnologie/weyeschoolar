<?php 
namespace App\Models\Entity;

class Eleve
{
    
    
    private $_matriculeEleve;
    private $_statutTuteur	;
    private $_tuteur;
    private $_phoneTuteur;	
    private $_idPersonne;	
    private $_idAnnee;


  

    function __construct()
    {
        
    }
    
    function getMatriculeEleve(){
        return $this->_matriculeEleve;
    }
    function setMatriculeEleve($matricule){
        $this->_matriculeEleve= $matricule;
    }
    function getPhoneTuteur(){
        return $this->_phoneTuteur;
    }
    function setPhoneTuteur($phone){
        $this->_phoneTuteur = $phone;
    }
    function getStatutTuteur(){
        return $this->_statutTuteur;
    }
    function setStatutTuteur($statut){
        $this->_statutTuteur = $statut;
    }
    function getTuteur(){
        return $this->_tuteur;
    }
    function setTuteur($tuteur){
        $this->_tuteur = $tuteur;
    }
    function getIdPersonne(){
        return $this->_idPersonne;
    }
    function setIdPersonne($id){
        $this->_idPersonne = $id;
    }
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }

}


?>