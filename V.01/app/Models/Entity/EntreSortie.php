<?php 
namespace App\Models\Entity;

class EntreSortie
{
    
  
    private $_idEntresortie	;
    private $_typeOperation	;
    private $_motif;
    private $_montant;	
    private $_dateEntresortie;	
    private $_concerne;	
    private $_idUser;
    private $_idExtension;	
    private $_idAnnee;
    private $_observation;


  

    function __construct()
    {
        
    }
    
    function getIdEntresortie(){
        return $this->_idEntresortie;
    }
    function setEntresortie($id){
        $this->_idEntresortie= $id;
    }
    function getTypeOperation(){
        return $this->_typeOperation;
    }
    function setTypeOperation($type){
        $this->_typeOperation = $type;
    }
    function getMotif(){
        return $this->_motif;
    }
    function setMotif($motif){
        $this->_motif = $motif;
    }
    function getMontant(){
        return $this->_montant;
    }
    function setMontant($montant){
        $this->_montant = $montant;
    }
    function getDateEntresortie(){
        return $this->_dateEntresortie;
    }
    function setDateEntresortie($date){
        $this->_dateEntresortie = $date;
    }
    function getConcerne(){
        return $this->_concerne;
    }
    function setConcerne($concerne){
        $this->_concerne = $concerne;
    }
    function getIdUser(){
        return $this->_idUser;
    }
    function setIdUser($user){
        $this->_idUser = $user;
    }
    function getIdExtension(){
        return $this->_idExtension;
    }
    function setIdExtension($id){
        $this->_idExtension = $id;
    }
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }
    function getObservation(){
        return $this->_observation;
    }
    function setObservation($observation){
        $this->_observation= $observation;
    }

}


?>