<?php 
namespace App\Models\Entity;

class Extension
{
    private  $_idExtension;
    private $_libelleExtension;
    private $_descriptionExtension;
    private $_idEcole;
  
    

    function __construct()
    {
        
    }
    function getIdExtension(){
        return $this->_idExtension;
    }
    function setIdExtension($id){
        $this->_idExtension = $id;
    }
    function getLibelleExtension(){
        return $this->_libelleExtension;
    }
    function setLibelleExtension($libelleExtension){
        $this->_libelleExtension = $libelleExtension;
    }
    function getDescriptionExtension(){
        return $this->_descriptionExtension;
    }
    function setDescriptionExtension($descriptionExtension){
        $this->_descriptionExtension = $descriptionExtension;
    }
    function getIdEcole(){
        return $this->_idEcole;
    }
    function setIdECole($id){
        $this->_idEcole = $id;
    }
  
  
  

}


?>