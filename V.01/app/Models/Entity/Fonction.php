<?php 
namespace App\Models\Entity;

class Fonction
{
    private  $_idFonction;
    private $_libelleFonction;
    private $_descriptionFonction;
    private $_idEcole;
  
    

    function __construct()
    {
        
    }
    function getIdFonction(){
        return $this->_idFonction;
    }
    function setIdFonction($id){
        $this->_idFonction= $id;
    }
    function getLibelleFonction(){
        return $this->_libelleFonction;
    }
    function setLibelleFonction($libellefonction){
        $this->_libelleFonction = $libellefonction;
    }
    function getDescriptionFonction(){
        return $this->_descriptionFonction;
    }
    function setDescriptionFonction($descriptionFonction){
        $this->_descriptionFonction = $descriptionFonction;
    }
  
  
  
  

}


?>