<?php 
namespace App\Models\Entity;

class FonctionEmploie
{
    

    private $_idFonctionEmploye	;
    private $_matriculeAgent	;
    private $_idFonction;
    private $_idAnnee;	
  	

    function __construct()
    {
        
    }
    
    function getIdFonctionEmploye(){
        return $this->_idFonctionEmploye;
    }
    function setIdFonctionEmploye($id){
        $this->_idFonctionEmploye= $id;
    }
    function getMatriculeAgent(){
        return $this->_matriculeAgent;
    }
    function setMatriculeAgent($matricule){
        $this->_matriculeAgent =$matricule;
    }
    function getIdFonction(){
        return $this->_idFonction;
    }
    function setIdFonction($id){
        $this->_idFonction = $id;
    }
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }
    

}


?>