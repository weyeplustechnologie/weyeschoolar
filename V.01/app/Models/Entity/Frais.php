<?php 
namespace App\Models\Entity;

class Frais
{
    private  $_idFrais;
    private  $_montant;
    private $_devise;
    private $_idAnnee;
    private $_idTypeFrais;
    private $_idCycle;
  
    

    function __construct()
    {
        
    }
    function getIdFrais(){
        return $this->_idFrais;
    }
    function setIdFrais($id){
        $this->_idFrais= $id;
    }
    function getMontant(){
        return $this->_montant;
    }
    function setMontant($montant){
        $this->_montant = $montant;
    }
    function getDevise(){
        return $this->_devise;
    }
    function setDevise($devise){
        $this->_devise = $devise;
    }
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }
    function getIdTypeFrais(){
        return $this->_idTypeFrais;
    }
    function setIdTypeFrais($id){
        $this->_idTypeFrais = $id;
    }
    function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($id){
        $this->_idCycle = $id;
    }
  
  
  

}


?>