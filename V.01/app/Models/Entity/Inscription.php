<?php 
namespace App\Models\Entity;

class Inscription
{
    

    private $_idInscription	;
    private $_idAnnee;
    private $_idClasse;
    private $_idCycle;
    private $_idOption;
    private $_idSection;
    private $_idExtension;
    private $_matriculeEleve;	
    private $_statutInscription;
  	

    function __construct()
    {
        
    }
    
    function getIdInscription(){
        return $this->_idInscription;
    }
    function setIdInscription($id){
        $this->_idInscription= $id;
    }
    function getMatriculeEleve(){
        return $this->_matriculeEleve;
    }
    function setMatriculeEleve($matricule){
        $this->_matriculeEleve =$matricule;
    }
    function getIdClasse(){
        return $this->_idClasse;
    }
    function setIdClasse($id){
        $this->_idClasse = $id;
    }
    function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($id){
        $this->_idCycle = $id;
    }
    function getIdOption(){
        return $this->_idOption;
    }
    function setIdOption($id){
        $this->_idOption = $id;
    }
    function getIdSection(){
        return $this->_idSection;
    }
    function setIdSection($id){
        $this->_idSection = $id;
    }
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }
    function getIdExtension(){
        return $this->_idExtension;
    }
    function setIdExtension($id){
        $this->_idExtension = $id;
    }
    function getStatutInscription(){
        return $this->_statutInscription;
    }
    function setStatutInscription($statut){
        $this->_statutInscription = $statut;
    }
    

}


?>