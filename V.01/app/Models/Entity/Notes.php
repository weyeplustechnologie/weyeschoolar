<?php 
namespace App\Models\Entity;

class Notes
{

  private  $_idNotes;
  private  $_idCours;
  private  $_idClasse;
  private  $_idTypeSession;
  private  $_matriculeEleve;
  private  $_notesObtenu;

  
  function __construct()
    {
        
    }
	
    function getIdNotes(){
        return $this->_idNotes;
    }
    function setIdNotes($id){
        $this->_idNotes= $id;
    }
	
	function getIdCours(){
        return $this->_idCours;
    }
    function setIdCours($idCours){
        $this->_idCours= $idCours;
    }
	
	function getIdClasse(){
        return $this->_idClasse;
    }
    function setIdClasse($idClasse){
        $this->_idClasse= $idClasse;
    }
	
	function getIdTypeSession(){
        return $this->_idTypeSession;
    }
    function setIdTypeSession($idTypeSession){
        $this->_idTypeSession= $idTypeSession;
    }
	
	function getMatriculeEleve(){
        return $this->_matriculeeleve;
    }
    function setMatriculeEleve($matriculeeleve){
        $this->_matriculeEleve= $matriculeeleve;
    }
	
	function getNotesObtenu(){
        return $this->_notesObtenu;
    }
    function setNotesObtenu($notesObtenu){
        $this->_notesObtenu= $notesObtenu;
    }
	

}


?>