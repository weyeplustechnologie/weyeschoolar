<?php 
namespace App\Models\Entity;

class Option
{
    private  $_idOption;
    private $_libelleOption;
    private $_descriptionOption;
    private 	$_idSection;

    function __construct()
    {
        
    }
    
    function getIdOption(){
        return $this->_idOption;
    }
    function setIdOption($id){
        $this->_idOption = $id;
    }
    function getLibelleOption(){
        return $this->_libelleOption;
    }
    function setLibelleOption($libelle){
        $this->_libelleOption = $libelle;
    }
    function getDescriptionOption(){
        return $this->_descriptionOption;
    }
    function setDescriptionOption($libelle){
        $this->_descriptionOption = $libelle;
    }
    function getIdSection(){
        return $this->_idSection;
    }
    function setIdSection($id){
        $this->_idSection = $id;
    }
    

  

}


?>