<?php 
namespace App\Models\Entity;

class PaiementFrais
{
    

    private $_idPaiementfrais;
    private $_montantPaie;
    private $_datePaiement;
    private $_idAnnee;
    private $_idPaiementtranche;	
    private $_idTypefrais;
    private $_matriculeEleve;
    private $_idUser;
    private $_idCycle;
    private $_idExtension;
    private $_devise;
 							

    function __construct()
    {
        
    }
    
    function getIdPaiementfrais(){
        return $this->_idPaiementfrais;
    }
    function setIdPaiementfrais($id){
        $this->_idPaiementfrais= $id;
    }
    function getMatriculeEleve(){
        return $this->_matriculeEleve;
    }
    function setMatriculeEleve($matricule){
        $this->_matriculeEleve =$matricule;
    }
    function getMontant(){
        return $this->_montantPaie;
    }
    function setMontant($montant){
        $this->_montantPaie = $montant;
    }
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }
    function getDatePaiement(){
        return $this->_datePaiement;
    }
    function setDatePaiement($date){
        $this->_datePaiement = $date;
    }
    function getIdPaiementtranche(){
        return $this->_idPaiementtranche;
    }
    function setIdPaiementtranche($id){
        $this->_idPaiementtranche = $id;
    }
    function getIdTypefrais(){
        return $this->_idTypefrais;
    }
    function setIdTypefrais($id){
        $this->_idTypefrais = $id;
    }
    function getIdUser(){
        return $this->_idUser;
    }
    function setIdUsers($id){
        $this->_idUser = $id;
    }
    function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($id){
        $this->_idCycle = $id;
    }
    function getIdExtension(){
        return $this->_idExtension;
    }
    function setIdExtension($id){
        $this->_idExtension = $id;
    }
    function getDevise(){
        return $this->_devise;
    }
    function setDevise($devise){
        $this->_devise = $devise;
    }

}


?>