<?php 
namespace App\Models\Entity;

class PaiementTranche
{
    
    				
    private $_idPaiementTranche;
    private $_montant;
    private $_devise;
    private $_idAnnee;
    private $_idTranche;
    private $_idTypefrais;	
    private $_idCycle;
    
 							

    function __construct()
    {
        
    }
    
    function getIdPaiementtranche(){
        return $this->_idPaiementTranche;
    }
    function setIdPaiementtranche($id){
        $this->_idPaiementTranche = $id;
    }
    function getMontant(){
        return $this->_montant;
    }
    function setMontant($montant){
        $this->_montant = $montant;
    }
    function getDevise(){
        return $this->_devise;
    }
    function setDevise($devise){
        $this->_devise = $devise;
    }
    function getIdAnnee(){
        return $this->_idAnnee;
    }
    function setIdAnnee($id){
        $this->_idAnnee = $id;
    }
    function getIdTranche(){
        return $this->_idTranche;
    }
    function setIdTranche($id){
        $this->_idTranche = $id;
    }
  
    function getIdTypefrais(){
        return $this->_idTypefrais;
    }
    function setIdTypefrais($id){
        $this->_idTypefrais = $id;
    }
    function getIdCycle(){
        return $this->_idCycle;
    }
    function setIdCycle($id){
        $this->_idCycle = $id;
    }
  
  

}


?>