<?php 
namespace App\Models\Entity;

class Personne
{
    								
    private $_idPersonne;			
    private $_nom;
    private $_postnom;
    private $_prenom;
    private $_sexe;
    private $_lieuNaissance;	
    private $_dateNaissance;
    private $_province;
    private $_nationalite;
    private $_idAdresse;
 							

    function __construct()
    {
        
    }
    
    function getIdPersonne(){
        return $this->_idPersonne;
    }
    function setIdPersonne($id){
        $this->_idPersonne = $id;
    }
    function getNom(){
        return $this->_nom;
    }
    function setNom($nom){
        $this->_nom = $nom;
    }
    function getPostnom(){
        return $this->_postnom;
    }
    function setPostnom($postnom){
        $this->_postnom = $postnom;
    }
    function getPrenom(){
        return $this->_prenom;
    }
    function setPrenom($prenom){
        $this->_prenom = $prenom;
    }
    function getSexe(){
        return $this->_sexe;
    }
    function setSexe($sexe){
        $this->_sexe = $sexe;
    }
  
    function getLieuNaissance(){
        return $this->_lieuNaissance;
    }
    function setLieuNaissance($lieuNaissance){
        $this->_lieuNaissance = $lieuNaissance;
    }
    function getDateNaissance(){
        return $this->_dateNaissance;
    }
    function setDateNaissance($dateNaissance){
        $this->_dateNaissance = $dateNaissance;
    }
    function getProvince(){
        return $this->_province;
    }
    function setProvince($province){
        $this->_province = $province;
    }
    function getNationalite(){
        return $this->_nationalite;
    }
    function setNationalite($nationalite){
        $this->_nationalite = $nationalite;
    }
    function getIdAdresse(){
        return $this->_idAdresse;
    }
    function setIdAdresse($id){
        $this->_idAdresse = $id;
    }
  

}


?>