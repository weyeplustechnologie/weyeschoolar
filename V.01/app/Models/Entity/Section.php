<?php 
namespace App\Models\Entity;

class Section
{
    private  $_idSection;
    private $_libelleSection;
    private $_descriptionSection;
    private $_idCycle;

    function __construct()
    {
        
    }
    
    function getIdSection(){
        return $this->_idSection;
    }
    function setIdSection($id){
        $this->_idSection = $id;
    }
  
    function getLibelleSection(){
        return $this->_libelleSection;
    }
    function setLibelleSection($libelle){
        $this->_libelleSection = $libelle;
    }
    function getDescriptionSection(){
        return $this->_descriptionSection;
    }
    function setDescriptionSection($libelle){
        $this->_descriptionSection = $libelle;
    }
    function getIdCyle(){
        return $this->_idCycle;
    }
    function setIdCyle($id){
        $this->_idCycle = $id;
    }

  

}


?>