<?php 
namespace App\Models\Entity;

class Tranche
{
    private  $_idTranche;
    private  $_libelleTranche;
    private $_idTypeFrais;
  
    

    function __construct()
    {
        
    }
    function getIdTranche(){
        return $this->_idTranche;
    }
    function setIdTanche($id){
        $this->_idTranche= $id;
    }
    function getLibelleTranche(){
        return $this->_libelleTranche;
    }
    function setLibelleTranche($libelle){
        $this->_libelleTranche =$libelle;
    }
    function getIdTypeFrais(){
        return $this->_idTypeFrais;
    }
    function setIdTypeFrais($id){
        $this->_idTypeFrais = $id;
    }
  
  
  

}


?>