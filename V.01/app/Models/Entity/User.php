<?php
namespace App\Models\Entity;

    class User 
    {
        protected $_identifiant;
        protected $_matricule;
        protected $_password;
        protected $_statut;
		protected $_createOn;
		protected $_role;
		protected $_createBy;
		protected $_idExtension;

        public function __construct() // Constructeur
        {
        
        }
        public function getIdentifiant()
		{

			return $this->_identifiant;
		}
	    public function setIdentifiant($_identifiant)
		{
			
			$this->_identifiant = $_identifiant;
        }
        public function getMatricule()
		{

			return $this->_matricule;
		}
	    public function setMatricule($_matricule)
		{
			
			$this->_matricule = $_matricule;
        }
        public function getPassword()
		{

			return $this->_password;
		}
	    public function setPassword($_password)
		{
			
			$this->_password = $_password;
        }
        public function getStatut()
		{

			return $this->_statut;
		}
	    public function setStatut($statut)
		{
			
			$this->_statut = $statut;
        }
        public function getCreateOn()
		{

			return $this->_createOn;
		}
	    public function setCreateOn($_createOn)
		{
			
			$this->_createOn = $_createOn;
		}
		public function getIdExtension()
		{

			return $this->_idExtension;
		}
	    public function setIdExtension($site)
		{
			
			$this->_idExtension = $site;
		}
		public function getRole()
		{

			return $this->_role;
		}
	    public function setRole($_role)
		{
			
			$this->_role = $_role;
		}
		public function getCreateBy()
		{
			
			return $this->_createBy ;
		}
		public function setCreateBy($_createBy)
		{
			
			$this->_createBy = $_createBy;
		}
    }
    
?>