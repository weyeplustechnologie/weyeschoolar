<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Api-token, Authorization, X-Requested-With");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, HEAD');
header("content-type: application/json");
use Router\Router;
require 'vendor/autoload.php';

$router = new Router($_GET["url"]);
$router->post("ecole/add","App\Controllers\EcoleRestController@add");
$router->get("ecole","App\Controllers\EcoleRestController@getAll");
$router->post("extension/add","App\Controllers\ExtensionRestController@add");
$router->get("extension","App\Controllers\ExtensionRestController@getAll");
$router->post("user/add","App\Controllers\UserRestController@add");
$router->get("users","App\Controllers\UserRestController@getAll");
$router->post("Annee/add","App\Controllers\AnneeRestController@add");
$router->get("Annee","App\Controllers\AnneeRestController@getAll");
$router->post("Cycle/add","App\Controllers\CycleRestController@add");
$router->get("Cycle","App\Controllers\CycleRestController@getAll");
$router->post("Section/add","App\Controllers\SectionRestController@add");
$router->get("Section","App\Controllers\SectionRestController@getAll");
$router->post("Option/add","App\Controllers\OptionRestController@add");
$router->get("Option","App\Controllers\OptionRestController@getAll");
$router->post("Division/add","App\Controllers\DivisionRestController@add");
$router->get("Division","App\Controllers\DivisionRestController@getAll");
$router->post("Classe/add","App\Controllers\ClasseRestController@add");
$router->get("Classe","App\Controllers\ClasseRestController@getAll");
$router->post("Cours/add","App\Controllers\CoursRestController@add");
$router->get("Cours","App\Controllers\CoursRestController@getAll");
$router->post("Epreuve/add","App\Controllers\EpreuveRestController@add");
$router->get("Epreuve","App\Controllers\EpreuveRestController@getAll");
$router->post("Typefrais/add","App\Controllers\TypeFraisRestController@add");
$router->get("Typefrais","App\Controllers\TypeFraisRestController@getAll");
$router->post("Tranchefrais/add","App\Controllers\TrancheFraisRestController@add");
$router->get("Tranchefrais","App\Controllers\TrancheFraisRestController@getAll");
$router->post("Frais/add","App\Controllers\FraisRestController@add");
$router->get("Frais","App\Controllers\FraisRestController@getAll");
$router->post("Fonction/add","App\Controllers\FonctionRestController@add");
$router->get("Fonction","App\Controllers\FonctionRestController@getAll");
$router->post("PaiementTranche/add","App\Controllers\PaiementTrancheFraisRestController@add");
$router->get("PaiementTranche","App\Controllers\PaiementTrancheFraisRestController@getAll");
$router->post("Agent/add","App\Controllers\AgentRestController@add");
$router->get("Agent","App\Controllers\AgentRestController@getAll");
$router->get("Agent/All/:idfonction","App\Controllers\AgentRestController@getAllAgentbyFonction");
$router->get("Agent/getMatricule","App\Controllers\AgentRestController@getMatricule");
$router->post("DispenseCours/add","App\Controllers\AttribuerCoursRestController@add");
$router->get("DispenseCours","App\Controllers\AttribuerCoursRestController@getAll");
$router->post("Eleve/add","App\Controllers\EleveRestController@add");
$router->get("Eleve/getMatricule","App\Controllers\EleveRestController@getMatricule");
$router->get("Eleve/AllActive","App\Controllers\EleveRestController@getAllActive");
$router->post("Paid/add","App\Controllers\PaiementFraisRestController@Add");
$router->post("EntreSorti/add","App\Controllers\EntreSortiRestController@Add");
$router->post("Login","App\Controllers\UserRestController@login");



$router->run();

?>